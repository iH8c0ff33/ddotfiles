{ inputs, cell }:
let inherit (inputs.cells.haumea.lib) loaders;
in inputs.haumea.lib.load {
  inputs = { inherit inputs cell; };
  src = ./lib;
  loader = loaders.sensible;
}
