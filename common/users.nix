{ inputs, cell }:
let
  inherit (inputs.haumea.lib) load;
  inherit (inputs.cells.haumea.lib) loaders transformers;
in load {
  inputs = { inherit inputs cell; };
  src = ./users;

  loader = loaders.sensible;
  transformer = transformers.liftDefaultOnly;
}
