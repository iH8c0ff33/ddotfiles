{ inputs, cell }:
let inherit (inputs.nixpkgs.lib) elem getName;
in {
  # Allowed unfree packages
  allowUnfreePredicate = pkg:
    elem (getName pkg) [
      # NVIDIA
      "nvidia-x11"
      "nvidia-settings"
      # Steam
      "steam"
      "steam-original"
      "steam-run"
    ];
}
