{ inputs, cell }:
let inherit (inputs.cells.common.lib) getUserHome;
in { inputs, config, lib, pkgs, ... }:
let
  inherit (builtins) hasAttr;
  inherit (lib) optional optionalAttrs;
  inherit (pkgs.stdenv) hostPlatform;
  inherit (config.home-manager.users.${username}.identity) name sshPublicKeys;

  username = "sara";
in {
  users.users.${username} = {
    description = name;
    shell = pkgs.fish;
    home = getUserHome username hostPlatform;
  } // optionalAttrs hostPlatform.isLinux {
    isNormalUser = true;
    hashedPassword =
      "$6$40mxD4hcACWGUSHM$gGXs1K4wswjvQ9IFgegBSkwP.yTyYDlWP1Nf0dbeTVFFEqHbmsfKYkYPW8bwHz6m/yblaJMY0/GZyizR0Q4qW/";
    extraGroups = [ "wheel" ]
      ++ optional config.networking.networkmanager.enable "networkmanager"
      ++ optional config.virtualisation.docker.enable "docker"
      ++ optional config.virtualisation.libvirtd.enable "libvirtd"
      ++ optional config.hardware.pulseaudio.enable "audio"
      ++ optional (hasAttr "wireshark" config.users.groups) "wireshark";
    openssh.authorizedKeys.keys = sshPublicKeys;
  };
  programs.fish.enable = true;
}
