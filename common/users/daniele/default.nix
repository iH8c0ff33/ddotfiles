{ inputs, cell }:
let inherit (inputs.cells.common.lib) getUserHome;
in { inputs, config, lib, pkgs, ... }:
let
  inherit (builtins) hasAttr;
  inherit (lib) optional optionalAttrs;
  inherit (pkgs.stdenv) hostPlatform;
  inherit (config.home-manager.users.daniele.identity) name sshPublicKeys;

  username = "daniele";
in {
  users.users.${username} = {
    description = name;
    shell = pkgs.fish;
    home = getUserHome username hostPlatform;
  } // optionalAttrs hostPlatform.isLinux {
    isNormalUser = true;
    hashedPassword =
      "$6$4QgX0Wti.6G2Zqe$ZIK2CHsnUcIi.Z4rpMKejQogOYFT4EFwjLB3XP2YrDb0BHu2u6F6TuAWSUgfBu9C8P5LTkaEIsLecWyX3IZAF/";
    extraGroups = [ "wheel" ]
      ++ optional config.networking.networkmanager.enable "networkmanager"
      ++ optional config.virtualisation.docker.enable "docker"
      ++ optional config.virtualisation.libvirtd.enable "libvirtd"
      ++ optional config.hardware.pulseaudio.enable "audio"
      ++ optional (hasAttr "wireshark" config.users.groups) "wireshark";
    openssh.authorizedKeys.keys = sshPublicKeys;
  };
  programs.fish.enable = true;
  # fonts.fonts = with pkgs; [
  #   fira-code
  #   iosevka-bin
  #   jetbrains-mono
  #   roboto-mono
  # ];
}
