{ lib, pkgs, ... }:

{
  hardware.pulseaudio.enable = lib.mkForce false;
  services = {
    jack.jackd.enable = lib.mkForce false;
    pipewire = {
      enable = true;
      alsa.enable = true;
      jack.enable = true;
      pulse.enable = true;
    };
  };

  environment.systemPackages = with pkgs; [
    alsaUtils
    ncpamixer
    pavucontrol
  ];
}
