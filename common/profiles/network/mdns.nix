{
  services.avahi = {
    enable = true;
    nssmdns4 = true;
    reflector = true;
    allowPointToPoint = true;
    ipv4 = true;
    ipv6 = true;
    publish.enable = true;
    publish.addresses = true;
  };
}
