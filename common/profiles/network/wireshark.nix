{ config, lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs) hostPlatform wireshark;
in
{
  environment.systemPackages = [ wireshark ];

  security = mkIf hostPlatform.isLinux {
    wrappers.dumpcap = {
      source = "${wireshark}/bin/dumpcap";
      capabilities = "cap_net_raw+p";
      owner = "root";
      group = "wireshark";
      permissions = "u+rx,g+x";
    };
  };

  users.groups.wireshark = { };
}
