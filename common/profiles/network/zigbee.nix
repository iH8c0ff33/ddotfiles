{ ... }: {
  imports = [ ./wireshark.nix ];

  # CC2531 zigbee sniffer
  services.udev.extraRules = ''
    SUBSYSTEM=="usb", ATTRS{idVendor}=="0451", ATTR{idProduct}=="16ae", GROUP="wireshark"
  '';
}
