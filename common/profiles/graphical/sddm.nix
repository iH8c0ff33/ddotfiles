{ ... }: {
  imports = [ ./core.nix ];

  services.xserver.displayManager.sddm.enable = true;
}
