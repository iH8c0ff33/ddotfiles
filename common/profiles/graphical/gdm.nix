{ ... }: {
  imports = [ ./core.nix ];

  services.xserver.displayManager.gdm.enable = true;
}
