{ ... }: {
  # Plymouth splash screen
  boot.plymouth.enable = true;

  # At least for now, xserver is needed
  services.xserver.enable = true;
}
