{ pkgs, ... }: {
  environment.systemPackages = with pkgs; with gnomeExtensions; [ pop-shell ];

  environment.gnome.excludePackages = with pkgs; [
    gnome3.epiphany
  ];

  programs.gnome-terminal.enable = true;
  programs.ssh.askPassword = "${pkgs.gnome.seahorse}/libexec/seahorse/ssh-askpass";

  services.xserver = {
    desktopManager.gnome.enable = true;
  };
}
