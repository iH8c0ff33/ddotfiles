{ lib, ... }:
let inherit (lib) mkDefault;
in {
  boot.plymouth.enable = mkDefault true;
  hardware.pulseaudio.enable = true;
  services = {
    displayManager.sddm = {
      enable = true;
      wayland.enable = true;
    };
    desktopManager.plasma6.enable = true;
  };
}
