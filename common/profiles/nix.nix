{ lib, pkgs, ... }:
let
  inherit (builtins) attrNames attrValues;
  inherit (lib) mkDefault mkIf mkMerge;
  inherit (pkgs.stdenv.hostPlatform) isDarwin isLinux;

  caches = {
    "https://nix-community.cachix.org" =
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs=";
    "https://colmena.cachix.org" =
      "colmena.cachix.org-1:7BzpDnjjH8ki2CT3f6GdOk7QAzPOl+1t3LvTLXqYcSg=";
    "https://mic92.cachix.org" =
      "mic92.cachix.org-1:gi8IhgiT3CYZnJsaW7fxznzTkMUOn1RY4GmXdT/nXYQ=";
  };
in {
  nix = {
    package = pkgs.nixVersions.latest;

    nrBuildUsers = 0;

    gc = {
      automatic = mkDefault true;
      options = "--delete-older-than 7d";
    };

    # NOTE: default is already true
    # useSandbox = mkDefault true;

    # # flake-utils-plus
    # linkInputs = true;
    # generateRegistryFromInputs = true;
    # generateNixPathFromInputs = true;

    settings = {
      allowed-users = mkIf isLinux [ "@wheel" ];
      trusted-users = mkMerge [
        [ "root" ]
        (mkIf isLinux [ "@wheel" ])
        (mkIf isDarwin [ "@admin" ])
      ];

      auto-optimise-store = mkDefault true;

      builders-use-substitutes = true;
      substituters = attrNames caches;
      trusted-public-keys = attrValues caches;

      experimental-features = mkMerge [
        [
          "nix-command"
          "flakes"
          "auto-allocate-uids"
          # TODO: try enabling
          # "ca-derivations"
        ]
        (mkIf isLinux [ "cgroups" ])
      ];
      auto-allocate-uids = true;
      use-cgroups = mkIf isLinux true;
      sandbox = mkDefault true;
    };
  };
}
