{
  imports = [ ./defaults.nix ];

  nix = {
    configureBuildUsers = true;

    # TODO: should be fixed now, check
    # nixPath = [
    #   # TODO: This entry should be added automatically via FUP's `nix.linkInputs`
    #   # and `nix.generateNixPathFromInputs` options, but currently that doesn't
    #   # work because nix-darwin doesn't export packages, which FUP expects.
    #   #
    #   # This entry should be removed once the upstream issues are fixed.
    #   #
    #   # https://github.com/LnL7/nix-darwin/issues/277
    #   # https://github.com/gytis-ivaskevicius/flake-utils-plus/issues/107
    #   "darwin=/etc/nix/inputs/darwin"
    # ];
  };

  services = {
    activate-system.enable = true;
    nix-daemon.enable = true;
  };

  # Used for backwards compatibility, please read the changelog before changing.
  # https://daiderd.com/nix-darwin/manual/index.html#opt-system.stateVersion
  # $ darwin-rebuild changelog
  system.stateVersion = 4;
}
