{ lib, pkgs, profiles, ... }:
let inherit (lib) mkDefault; in
{
  imports = [ profiles.misc.udev-nosettle ];

  boot = {
    kernelPackages = mkDefault pkgs.linuxPackages_latest;
    kernel.sysctl."kernel.sysrq" = 1;
    supportedFilesystems = [ "ntfs" ];
  };
  zramSwap.enable = true;

  console.useXkbConfig = true;
  i18n.defaultLocale = mkDefault "en_US.UTF-8";

  fonts = {
    fonts = with pkgs; [ dejavu_fonts ];

    fontconfig.defaultFonts = {
      monospace = [ "DejaVu Sans Mono" ];
      sansSerif = [ "DejaVu Sans" ];
    };
  };

  # I don't like it (it's slow!)
  programs.command-not-found.enable = false;

  services.earlyoom.enable = true;

  # Declarative users!
  users.mutableUsers = false;
}
