{ lib, pkgs, ... }:
let
  inherit (lib) optionals;
  inherit (pkgs.stdenv) hostPlatform;
in
{
  environment = {
    variables = {
      BAT_THEME = "ansi";
      BAT_PAGER = "less";
      PAGER = "less";
      LESS = "-iFMRWX -z-4 -x4";
      LESSOPEN = "|${pkgs.lesspipe}/bin/lesspipe.sh %s";
      EDITOR = "vim";
      VISUAL = "vim";
    };

    shellAliases = {
      cat = "${pkgs.bat}/bin/bat"; # pretty (colorful) cat

      # Hi computer! I'm a human.
      df = "df -h";
      du = "du -h";

      ls = "eza"; # pretty ls (same as above)
      l = "ls -lg --git";
      la = "l -a"; # for when I want to see EVERYTHING!
      t = "l -T"; # I like trees
      ta = "l -Ta"; # I like more trees with all the leaves...
    };

    systemPackages = with pkgs; [
      bat
      eza
      file
      git-crypt
      gnupg
      less
      lsof
      procs
      vim
    ] ++ optionals hostPlatform.isLinux [ dstat ncdu ];
  };
}
