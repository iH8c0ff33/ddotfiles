{ lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs.stdenv) hostPlatform;
in
{
  imports = [ ../common ];

  environment = {
    systemPackages = with pkgs; [
      direnv
    ];

    shells = mkIf hostPlatform.isDarwin [ pkgs.fish ];
  };

  programs.fish.enable = true;
}
