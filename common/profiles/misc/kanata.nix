{
  services.kanata = {
    enable = true;
    # This cfg remaps caps lock to escape when alone and lctl when in a chord
    keyboards.default = {
      config = ''
        (defsrc caps)
        (defalias caps (tap-hold-press 200 200 esc lctl))
        (deflayer base @caps)
      '';
      extraDefCfg = "process-unmapped-keys yes";
    };
  };
}
