{ pkgs, ... }: {
  # NOTE: quoting systemd documentation:
  # > Using this service is not recommended. There can be no guarantee that
  # > hardware is fully discovered at any specific time, because the kernel does
  # > hardware detection asynchronously, and certain buses and devices take a very
  # > long time to become ready, and also additional hardware may be plugged in at
  # > any time. Instead, services should subscribe to udev events and react to any
  # > new hardware as it is discovered. Services that, based on configuration,
  # > expect certain devices to appear, may warn or report failure after a
  # > timeout. This timeout should be tailored to the hardware type. Waiting for
  # > systemd-udev-settle.service usually slows boot significantly, because it
  # > means waiting for all unrelated events too.

  systemd.services.systemd-udev-settle.serviceConfig.ExecStart = [
    ""
    "${pkgs.coreutils}/bin/true"
  ];
}
