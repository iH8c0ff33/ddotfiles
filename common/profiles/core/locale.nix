{ lib, ... }:
let inherit (lib) mkDefault;
in { time.timeZone = mkDefault "Europe/Rome"; }
