{ lib, pkgs, ... }:
let
  inherit (lib) mkIf mkMerge;
  inherit (pkgs.hostPlatform) isDarwin isLinux;
in {
  environment.systemPackages = with pkgs;
    mkMerge [
      [ coreutils curl fd fx git jq nix-diff nix-index ripgrep rsync yq ]
      (mkIf isLinux [
        # linux utilities
        binutils
        dnsutils
        iputils
        man-pages
        ncdu
        stdmanpages
        utillinux
        whois

        # hw tools
        acpi
        acpitool
        brightnessctl
        lm_sensors
        nvme-cli
        pciutils
        smartmontools
        usbutils

        # fs tools
        exfat
        dosfstools
        gptfdisk
        parted
      ])
      (mkIf isDarwin [ bintools ])
    ];
}
