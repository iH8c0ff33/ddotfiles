{ inputs, cell }:
let
  inherit (inputs.cells.haumea.lib.transformers) liftDefaultOnly;
  inherit (inputs.haumea.lib) load loaders;
in load {
  inputs = { inherit inputs cell; };
  src = ./profiles;
  transformer = liftDefaultOnly;
  loader = loaders.verbatim;
}
