{ inputs, cell }:
with cell.profiles; {
  base = [ core.locale core.packages nix ];
  game = [ gaming ];
  gnome = [ graphical.gdm graphical.gnome ];
  virtualisation = [ virt.libvirt virt.podman ];
}
