{ inputs, cell }:
let
  default = {
    input = inputs.nixpkgs;
    home = inputs.home-manager;
    darwin = inputs.nix-darwin;

    config = cell.nixpkgsConfig;
    overlays = [ inputs.neovim-nightly-overlay.overlays.default ];
  };
in {
  stable = default // {
    input = inputs.nixpkgs-stable;
    home = inputs.home-manager-stable;
  };

  unstable = default // {
    input = inputs.nixpkgs-unstable;
    home = inputs.home-manager-unstable;
  };
}
