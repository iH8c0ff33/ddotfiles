{ inputs, cell }:
{ channel ? cell.channels.stable, config ? cell.nixpkgsConfig, overlays ? [ ]
, system }:
{ lib, options, ... }: {
  bee = {
    inherit system;
    inherit (channel) darwin home;

    pkgs = import channel.input {
      inherit system;

      overlays = channel.overlays ++ overlays;
      config = channel.config // config;
    };
  };
  nixpkgs = lib.optionalAttrs (builtins.hasAttr "flake" options.nixpkgs) {
    flake.source = channel.input.outPath;
  };
}

