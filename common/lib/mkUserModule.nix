{ inputs, cell }:
let
  inherit (cell) users;
  inherit (cell.lib) extendModule getUserHome;
  homeUsers = inputs.cells.home.users;
in username: homeProfiles: {
  imports = [
    users.${username} or (builtins.trace
      "user ${username} doesn't have a defined module, using defaults"
      ({ pkgs, ... }:
        let inherit (pkgs.stdenv) hostPlatform;
        in {
          users.users.${username} = {
            description = username;
            home = getUserHome username hostPlatform;
          };
        }))
  ];
  home-manager.users.${username} =
    extendModule (homeUsers.${username} or { }) homeProfiles;
}
