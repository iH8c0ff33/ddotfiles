module: extraModules:
let inherit (builtins) isAttrs;
in if isAttrs module then
  module // { imports = (module.imports or [ ]) ++ extraModules; }
else {
  imports = [ module ] ++ extraModules;
}
