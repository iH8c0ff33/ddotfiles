let
  inherit (builtins) all attrNames functionArgs hasAttr isFunction mapAttrs;
  traceIf = cond: if cond then builtins.trace else _: a: a;
in {
  debug = false;
  importer = import;

  __functor = self: inputs: path:
    let
      imported = self.importer path;
      args = functionArgs imported;
    in if isFunction imported && args != { }
    && all (attr: hasAttr attr inputs) (attrNames args) then

      (traceIf self.debug "loading ${path} with inputs" imported)
      (mapAttrs (name: _: inputs.${name}) args)
    else
      imported;
}
