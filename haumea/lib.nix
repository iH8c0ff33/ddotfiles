{ inputs, cell }:
let inherit (inputs.haumea.lib) load loaders;
in load {
  src = ./lib;
  loader = loaders.verbatim;
}
