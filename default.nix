{ inputs, config, lib, pkgs, ... }:
# Common config for all hosts
let
  inherit (inputs) nixpkgs nixpkgs-unstable darwin home;
  inherit (lib) optional optionalAttrs;
  inherit (lib.my) dotFilesDir mapModulesRec';
  inherit (pkgs.stdenv) hostPlatform;
in
{
  # import personal modules
  imports = (mapModulesRec' (toString ./modules) import);

  environment.variables.DDOTFILES = dotFilesDir;

  # Nix configuration
  environment.variables.NIXPKGS_ALLOW_UNFREE = "1"; # Sorry again, Rich...
  nix = {
    package = pkgs.nixFlakes; # Enable nix flakes (unstable)
    extraOptions = "experimental-features = nix-command flakes";
    nixPath = [
      "nixpkgs=${nixpkgs}"
      "nixpkgs-unstable=${nixpkgs-unstable}"
      "nixpkgs-overlays=${dotFilesDir}/overlays"
      "home-manager=${home}"
      "dotfiles=${dotFilesDir}"
    ] ++ optional hostPlatform.isDarwin "darwin=${darwin}";
    binaryCaches =
      [ "https://cache.nixos.org/" "https://nix-community.cachix.org" ];
    binaryCachePublicKeys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
    ];
  } // (optionalAttrs hostPlatform.isLinux {
    registry = {
      nixos.flake = nixpkgs;
      nixpkgs.flake = nixpkgs-unstable;
    };
  });

  environment.systemPackages = with pkgs;
    [ coreutils git vim gnumake ]
    ++ optional hostPlatform.isLinux cached-nix-shell;

  users.users.${config.user.name} = {
    home =
      let usersPath = if hostPlatform.isDarwin then "/Users" else "/home";
      in usersPath + "/${config.user.name}";
  } // optionalAttrs hostPlatform.isLinux {
    isNormalUser = true;
    extraGroups = [ "wheel" ];
  };
}
