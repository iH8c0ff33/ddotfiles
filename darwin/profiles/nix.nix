{ lib, ... }:
let inherit (lib) mkDefault;
in {
  nix.gc.interval = {
    Weekday = 0;
    Hour = 3;
    Minute = 30;
  };

  # Apparently, on nix-darwin multi-user mode is not enabled by default
  services.nix-daemon.enable = mkDefault true;
}
