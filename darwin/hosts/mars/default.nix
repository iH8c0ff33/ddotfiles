{ inputs, cell }:
let
  inherit (cell) suites;
  inherit (inputs.cells.common) channels;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
  homeSuites = inputs.cells.home.suites;
in { pkgs, ... }: {
  imports = suites.base ++ [
    (mkBee {
      system = "aarch64-darwin";
      channel = channels.unstable;
    })
    (mkUserModule "daniele" homeSuites.macbook)
    (mkUserModule "sara" homeSuites.macbook)
  ];

  networking.hostName = "mars";

  environment.etc."pam.d/sudo_local".text = ''
    # nix-darwin managed
    auth      optional      ${pkgs.pam-reattach}/lib/pam/pam_reattach.so ignore_ssh
    auth      sufficient    pam_tid.so
  '';

}
