{ inputs, cell }:
let
  inherit (inputs.cells.common) users;
  inherit (inputs.cells.common.lib) mkBee;
in {
  imports = [ (mkBee { system = "x86_64-darwin"; }) users.daniele ];

  networking.hostName = "naughty-darwin";

  # modules = {
  #   editors.emacs.enable = true;
  #   dev = {
  #     cc.enable = true;
  #     python.enable = true;
  #     rust.enable = true;
  #   };
  #   shell.zsh.z4h.enable = true;
  #   typeset.pandoc.enable = true;
  # };

  # List packages installed in system profile. To search by name, run:
  # $ nix-env -qaP | grep wget
  # environment.systemPackages = [
  #   pkgs.nodejs
  #   pkgs.nixpkgs-fmt
  #   pkgs.vscode
  #   pkgs.weechat
  #   (pkgs.neovim.override {
  #     vimAlias = true;
  #     configure = {
  #       packages.myPlugins = with pkgs.vimPlugins; {
  #         start =
  #           [ vim-nix coc-nvim coc-yaml coc-json nerdcommenter vim-surround ];
  #       };
  #     };
  #   })
  # ];

  # homebrew = {
  #   enable = true;
  #   autoUpdate = true;
  #   cleanup = "zap";

  #   global = {
  #     brewfile = true;
  #     noLock = true;
  #   };

  #   # TODO: Add macOS support to syncthing derivation
  #   brews = [ "syncthing" ];

  #   taps = [
  #     "homebrew/cask"
  #     "homebrew/cask-versions"
  #     "homebrew/core"
  #   ];

  #   casks = [
  #     "appcleaner"
  #     "docker-edge"
  #     "firefox-developer-edition"
  #     "openscad"
  #     "pock"
  #     "slack-beta"
  #     "telegram-desktop-dev"
  #     "transmission"
  #     "zoom"
  #   ];

  #   masApps = {
  #     Amphetamine = 937984704;
  #     Keepa = 1533805339;
  #     Xcode = 497799835;
  #   };
  # };

  # Use a custom configuration.nix location.
  # $ darwin-rebuild switch -I darwin-config=$HOME/.config/nixpkgs/darwin/configuration.nix
  # environment.darwinConfig = "$HOME/.config/nixpkgs/darwin/configuration.nix";

  # # Auto upgrade nix package and the daemon service.
  # services.nix-daemon.enable = true;
  # # nix.package = pkgs.nix;

  # # Create /etc/bashrc that loads the nix-darwin environment.
  # programs.zsh.enable = true; # default shell on catalina
  # programs.fish.enable = true;

  # # Used for backwards compatibility, please read the changelog before changing.
  # # $ darwin-rebuild changelog
  # system.stateVersion = 4;
}
