{ inputs, pkgs, ... }:
let
  inherit (inputs) deploy;
  inherit (pkgs) agenix cachix nixpkgs-fmt;
  pkgWithCategory = category: package: { inherit package category; };

  dnix = pkgWithCategory "dnix";
  linter = pkgWithCategory "linter";
in
{
  _file = toString ./.;
  commands = [
    (dnix agenix)
    (dnix deploy.packages.${pkgs.system}.deploy-rs)
    (dnix cachix)

    (linter nixpkgs-fmt)
  ];
}
