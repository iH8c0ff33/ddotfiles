{
  users = {
    daniele = [
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHtX8Gxh9qTph1sBHpg1+p32IAF5eynSxIofjeDGFGkA daniele@naughty-darwin.local.deutron.ml"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIJEJSk7lwAoYkONeSaMRlxZLeg9qCH/ihqMYXgajxIEt daniele@octopi.local.deutron.ml"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIAQr7nXZTU1exHP7JG+xr+dNqx3e/hc869POcg5D/flC daniele@tpp14s.local.deutron.ml"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIO6FLClrjCPI92lty/CCGT0DiTfZePdoNycpETc0cBtX daniele@workstation.local.deutron.ml"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIB5WIDaPAnkZGZoCg/sNAFenC24r8bPwsQNrDDXY9AkO daniele@h1.local.deutron.ml"
      "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKTStH1mE/aL8ZwhqlA1umEPk44c9GDRFLN+EIrBR5wc daniele@m1.priv.deutron.ml"
    ];
  };

  hosts = {
    octopi = [ "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIKTSbUKpmGqOUy9qnxF29EVEfqBB8gfm4RHSbbULZm/8 root@octopi" ];
  };
}
