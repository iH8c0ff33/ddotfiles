let
  pubkeys = import ../pubkeys;
in
{
  "pik3s_node-token.age".publicKeys = pubkeys.hosts.octopi;
}
