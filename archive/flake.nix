{
  description = "My nixos/darwin configuration.";

  nixConfig.extra-experimental-features = "nix-command flakes";
  nixConfig.extra-substituters = "https://nrdxp.cachix.org https://nix-community.cachix.org https://emacs.cachix.org";
  nixConfig.extra-trusted-public-keys = "nrdxp.cachix.org-1:Fc5PSqY2Jm1TrWfm88l6cvGWwz3s93c6IOifQWnhNW4= nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs= emacs.cachix.org-1:b1SMJNLY/mZF6GxQE+eDBeps7WnkT0Po55TAyzwOxTY=";

  inputs = {
    nixos-stable.url = "nixpkgs/nixos-23.11";
    nixos-unstable.url = "nixpkgs/nixos-unstable";
    nixpkgs-stable-darwin.url = "nixpkgs/nixpkgs-23.11-darwin";

    darwin.url = "github:LnL7/nix-darwin";
    darwin.inputs.nixpkgs.follows = "nixpkgs-stable-darwin";

    nur.url = "github:nix-community/nur";

    flake-utils-plus.url = "github:Princemachiavelli/flake-utils-plus/71b20cf";
    digga.url = "github:divnix/digga";
    digga.inputs = {
      darwin.follows = "darwin";
      flake-utils-plus.follows = "flake-utils-plus";
      nixpkgs.follows = "nixos-stable";
      nixlib.follows = "nixos-stable";
      home-manager.follows = "home";
      deploy.follows = "deploy";
    };

    flake-compat = {
      url = "github:edolstra/flake-compat";
      flake = false;
    };

    home.url = "github:nix-community/home-manager/release-23.11";
    home.inputs.nixpkgs.follows = "nixos-stable";

    # TODO: Remove once 190c6f4 is merged
    home-darwin.url = "github:nix-community/home-manager/master";
    home-darwin.inputs.nixpkgs.follows = "nixpkgs-stable-darwin";

    deploy.url = "github:serokell/deploy-rs";
    deploy.inputs.nixpkgs.follows = "nixos-stable";

    agenix.url = "github:ryantm/agenix";
    agenix.inputs.nixpkgs.follows = "nixos-stable";

    nvfetcher.url = "github:berberman/nvfetcher";
    nvfetcher.inputs.nixpkgs.follows = "nixos-stable";

    naersk.url = "github:nmattia/naersk";
    naersk.inputs.nixpkgs.follows = "nixos-stable";

    emacs-darwin.url = "github:cmacrae/emacs";
    nixos-generators.url = "github:nix-community/nixos-generators";
    nixos-hardware.url = "github:nixos/nixos-hardware";
    rust-overlay.url = "github:oxalica/rust-overlay";
    neovim-nightly-overlay.url = "github:nix-community/neovim-nightly-overlay";

    # NOTE: This is used by nix-darwin to instantiate the `pkg` arg, which is
    # later overridden by fup.
    nixpkgs.follows = "nixpkgs-stable-darwin";
  };

  outputs =
    { self
    , agenix
    , deploy
    , digga
    , emacs-darwin
    , home
    , home-darwin
    , naersk
    , neovim-nightly-overlay
    , nixos-unstable
    , nur
    , nvfetcher
    , ...
    } @ inputs:
    let
      inherit (digga.lib)
        collectHosts
        importExportableModules importHosts importOverlays
        mkDeployNodes mkFlake mkHomeConfigurations
        rakeLeaves;
    in
    mkFlake {
      inherit self inputs;

      channelsConfig = {
        allowUnfree = true;
      };

      channels = {
        nixos-stable = {
          imports = [
      	    (importOverlays ./overlays)
      	    (importOverlays ./pkgs/_overrides/nixos-stable)
      	  ];
      	};
        nixos-unstable = { };
        nixpkgs-stable-darwin = {
          imports = [
            (importOverlays ./overlays)
            (importOverlays ./pkgs/_overrides/nixpkgs-stable-darwin)
          ];
          overlays = [ emacs-darwin.overlay ];
        };
      };

      lib = import ./lib { lib = digga.lib // nixos-unstable.lib; };

      sharedOverlays = [
        (final: prev: {
          _dontExport = true;
          lib = prev.lib.extend (libFinal: libPrev: { our = self.lib; });
        })

        nur.overlay
        agenix.overlays.default
        naersk.overlay
        nvfetcher.overlays.default
        # neovim-nightly-overlay.overlays.default

        (import ./pkgs)
      ];

      nixos = {
        hostDefaults = {
          system = "x86_64-linux";
          channelName = "nixos-stable";
          imports = [ (importExportableModules ./modules) ];
          modules = [
            { lib.our = self.lib; }
            ({ suites, ... }: { imports = suites.base; })
            digga.nixosModules.bootstrapIso
            digga.nixosModules.nixConfig
            home.nixosModules.home-manager
            agenix.nixosModules.age
          ];
        };

        imports = [ (importHosts ./hosts/nixos) ];
        importables = rec {
          profiles = rakeLeaves ./profiles;
          suites = with profiles; {
            base = [ core linux nix ];
            game = [ gaming ];
            gnome = [ graphical.gdm graphical.gnome ];
            plasma = [ graphical.sddm graphical.plasma ];
            virtualisation = [ virt.libvirt virt.podman ];
          };
          users = rakeLeaves ./users;
        };
      };

      darwin = {
        hostDefaults = {
          system = "x86_64-darwin";
          channelName = "nixpkgs-stable-darwin";
          imports = [ (importExportableModules ./modules) ];
          modules = [
            { lib.our = self.lib; }
            ({ suites, ... }: { imports = suites.base; })
            home-darwin.darwinModules.home-manager
            agenix.nixosModules.age
          ];
        };

        imports = [ (importHosts ./hosts/darwin) ];
        importables = rec {
          profiles = rakeLeaves ./profiles;
          suites = with profiles; {
            base = [ core darwin nix shell.fish ];
          };
          users = rakeLeaves ./users;
        };

        hosts.m1.system = "aarch64-darwin";
      };

      home = {
        imports = [ (importExportableModules ./home/modules) ];
        modules = [ ];
        importables = rec {
          profiles = rakeLeaves ./home/profiles;
          suites = with profiles; {
            development = [ develop.direnv develop.git ];
	    devops = [ devops.containers devops.kubernetes ];
          };
          users = rakeLeaves ./home/users;
        };
        users = rakeLeaves ./home/users;
      };

      devshell = ./shell;

      homeConfigurations = mkHomeConfigurations
        (collectHosts self.nixosConfigurations self.darwinConfigurations);

      deploy.nodes = mkDeployNodes self.nixosConfigurations { };
    };
}
