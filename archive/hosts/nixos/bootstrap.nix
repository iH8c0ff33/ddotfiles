{ users, ... }: {
  imports = [
    users.root
    users.daniele
  ];

  config = {
    boot.loader.systemd-boot.enable = true;

    fileSystems."/".device = "/dev/disk/by-label/nixos";

    system.stateVersion = "22.11";
  };
}