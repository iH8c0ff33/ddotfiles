{ lib }: lib.makeExtensible (self: {
  getUserHome = username: platform:
    if platform.isDarwin
    then "/Users/${username}"
    else "/home/${username}";
})
