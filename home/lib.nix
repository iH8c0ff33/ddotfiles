{ inputs, cell }:
inputs.haumea.lib.load {
  inputs = { inherit inputs cell; };
  loader = inputs.cells.haumea.lib.loaders.sensible;
  src = ./lib;
}
