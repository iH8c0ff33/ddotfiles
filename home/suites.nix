{ inputs, cell }:
with cell.profiles;
let
  inherit (cell) profiles;
  development = [ develop.direnv develop.git develop.python ];
  devops = [
    profiles.devops.containers
    profiles.devops.kubernetes
    profiles.network.ssh
  ];
in rec {
  # core home-manager modules (imported in every user profile by the loader)
  coreModules = [ cell.modules.identity ];

  # device types
  desktop = [ browser.firefox network.syncthing ] ++ development ++ devops;
  laptop = desktop;
  macbook = laptop ++ [ security.secretive ];

  # profile groups
  inherit development devops;
}
