{
  programs.ssh.matchBlocks = {
    artaban = {
      hostname = "artaban.to.infn.it";
      user = "monteleo";
      identityFile = "~/.ssh/if_infn_ed";
    };

    zoroastro = {
      hostname = "zoroastro.to.infn.it";
      user = "monteleo";
      identityFile = "~/.ssh/id_infn";
    };

    cobbler-test = {
      hostname = "cobbler-test.to.infn.it";
      user = "monteleo";
      identityFile = "~/.ssh/id_infn_ed";
      proxyJump = "zoroastro";
    };

    awx-server = {
      hostname = "awx-server.priv.to.infn.it";
      user = "root";
      identityFile = "~/.ssh/id_infn_ed";
      proxyJump = "cobbler-test";
    };

    tmon = {
      hostname = "tmon.to.infn.it";
      user = "root";
      identityFile = "~/.ssh/id_infn_ed";
      proxyJump = "zoroastro";
    };

    "*.b8s.priv.to.infn.it" = {
      identityFile = "~/.ssh/id_infn_ed";
      proxyJump = "awx-server";
    };

    "*.prov.priv.to.infn.it" = {
      proxyJump = "awx-server";
    };

    bugernetes = {
      hostname = "bugernetes.to.infn.it";
      proxyJump = "zoroastro";
      user = "monteleo";
    };

    "o4bc-*" = {
      hostname = "%h.to.infn.it";
      proxyJump = "artaban";
      identityFile = "~/.ssh/id_infn_ed";
      user = "monteleo";
    };
  };
}
