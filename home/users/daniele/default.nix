{ inputs, cell }:
let
  inherit (inputs.cells.common) pubkeys;
  inherit (cell) profiles;
in { config, lib, pkgs, ... }:
let
  inherit (lib) optionalAttrs;
  inherit (pkgs.stdenv) hostPlatform;
  inherit (config.identity) name email pgpPublicKey;
in {
  imports = [ profiles.shell.fish ./ssh.nix ];

  identity = {
    name = "Daniele Monteleone";
    email = "daniele.monteleone.it@gmail.com";
    pgpPublicKey = pubkeys.gpg.daniele;
    sshPublicKeys = pubkeys.ssh.users.daniele;
  };

  home = {
    sessionVariables = rec {
      EDITOR = "nvim";
      VISUAL = EDITOR;
    };
    packages = with pkgs; [ age entr neovim ];
    username = "daniele";
    stateVersion = "22.11";
  };

  programs = {
    git = {
      userName = name;
      userEmail = email;
      signing.key = pgpPublicKey;
    };
  };

  # targets.genericLinux.enable = isGenericLinux && hostPlatform.isLinux;

  xdg = let inherit (config.home) homeDirectory;
  in {
    enable = true;

    cacheHome = "${homeDirectory}/.cache";
    configHome = "${homeDirectory}/.config";
    dataHome = "${homeDirectory}/.local/share";

    # configFile."nix/nix.conf".text = ''
    #   experimental-features = nix-command flakes
    # '';
    # configFile."nixpkgs/config.nix".text = ''
    #   {
    #       allowUnfree = true;
    #   }
    # '';
  } // optionalAttrs hostPlatform.isLinux {
    userDirs = {
      enable = true;

      desktop = "${homeDirectory}/desktop";
      documents = "${homeDirectory}/doc";
      download = "${homeDirectory}/tmp";
      music = "${homeDirectory}/var/music";
      pictures = "${homeDirectory}/var/images";
      publicShare = "${homeDirectory}/var/share";
      templates = "${homeDirectory}/templates";
      videos = "${homeDirectory}/var/videos";
    };
  };
}
