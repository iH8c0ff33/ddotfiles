{ inputs, cell }:
let inherit (cell) profiles;
in { config, pkgs, ... }: {
  imports = [ profiles.shell.fish ];

  identity = {
    name = "Sara Romano";
    email = "romanosara1999@gmail.com";
    sshPublicKeys = [ ];
  };

  home = {
    packages = with pkgs; [ age neovim ];
    sessionVariables = rec {
      EDITOR = "nvim";
      VISUAL = EDITOR;
    };
    username = "sara";
    stateVersion = "24.05";
  };

  programs = {
    git = let inherit (config.identity) name email;
    in {
      userName = name;
      userEmail = email;
    };
  };

  xdg = let inherit (config.home) homeDirectory;
  in {
    enable = true;

    cacheHome = "${homeDirectory}/.cache";
    configHome = "${homeDirectory}/.config";
    dataHome = "${homeDirectory}/.local/share";
  };
}
