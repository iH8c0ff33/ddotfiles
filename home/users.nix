{ inputs, cell }:
let
  inherit (inputs.haumea.lib) load;
  inherit (inputs.cells.haumea.lib.transformers) liftDefaultOnly;
in load {
  inputs = { inherit inputs cell; };
  src = ./users;

  loader = cell.lib.loadHomeUser;
  transformer = liftDefaultOnly;
}
