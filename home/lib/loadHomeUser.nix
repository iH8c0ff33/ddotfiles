{ inputs, cell }:
let inherit (inputs.haumea.lib) loaders;
in {
  loader = loaders.default;
  __functor = self: inputs: path: {
    imports = cell.suites.coreModules ++ [ (self.loader inputs path) ];
  };
}
