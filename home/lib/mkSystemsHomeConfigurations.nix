{ inputs, cell }:
let inherit (inputs.cells.common.lib) getUserHome;
in users:
let
  systems = [ "aarch64-darwin" "aarch64-linux" "x86_64-darwin" "x86_64-linux" ];

  op = attrs: system:
    builtins.foldl' (attrs: user:
      attrs // {
        "${user}-${system}" = { config, pkgs, ... }: {
          imports = [ users.${user} ];
          bee = {
            inherit system;
            pkgs = inputs.nixpkgs;
            home = inputs.home-manager;
          };

          home = {
            homeDirectory = getUserHome config.home.username pkgs.hostPlatform;
          };
        };
      }) attrs (builtins.attrNames users);
in builtins.foldl' op { } systems
