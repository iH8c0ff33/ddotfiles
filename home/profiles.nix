{ inputs, cell }:
let inherit (inputs.cells.haumea) lib;
in inputs.haumea.lib.load {
  inputs = { inherit inputs cell; };
  src = ./profiles;
  transformer = lib.transformers.liftDefaultOnly;
  loader = lib.loaders.sensible;
}
