{ pkgs, ... }: {
  home.packages = with pkgs; [
    poetry
    python310
    httpie
  ];
}
