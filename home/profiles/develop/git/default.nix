{ config, lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs.stdenv.hostPlatform) isDarwin;
in {
  home = {
    activation = {
      createGhqGitDirectory = lib.hm.dag.entryAfter [ "writeBoundary" ] ''
        run mkdir -p ${config.home.homeDirectory}/git
      '';
    };
    packages = with pkgs; [ ghq pre-commit ];
  };

  programs.git = {
    enable = true;

    aliases = {
      lg = "lg1";
      lg1 = "lg1s --all";
      lg2 = "lg2s --all";
      lg3 = "lg3s --all";

      lg1s =
        "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold green)(%ar)%C(reset) %C(auto)%s%C(reset) %C(dim)- %an%C(reset)%C(auto)%d%C(reset)'";
      lg2s =
        "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset)%C(auto)%d%C(reset)%n''          %C(auto)%s%C(reset) %C(dim)- %an%C(reset)'";
      lg3s =
        "log --graph --abbrev-commit --decorate --format=format:'%C(bold blue)%h%C(reset) - %C(bold cyan)%aD%C(reset) %C(bold green)(%ar)%C(reset) %C(bold cyan)(committed: %cD)%C(reset) %C(auto)%d%C(reset)%n''          %C(auto)%s%C(reset)%n'";
    };

    delta = {
      enable = true;
      options = {
        dark = {
          dark = true;
          syntax-theme = "OneHalfDark";
        };

        light = {
          light = true;
          syntax-theme = "OneHalfLight";
        };
      };
    };

    # signing.signByDefault = true;

    extraConfig = {
      pull.ff = "only";

      push = { default = "simple"; };

      format.pretty = "oneline";
      log = {
        abbrevCommit = true;
        decorate = "auto";
      };
      diff.tool = "nvimdiff";
      "difftool \"nvimdiff\"".cmd = ''nvim -d "$LOCAL" "$REMOTE"'';
      merge.tool = "vimdiff";
      rebase.autosquash = true;

      ghq.root = "~/git";
    };

    ignores = mkIf isDarwin [ ".DS_Store" ];
  };
}
