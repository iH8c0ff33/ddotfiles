{ config, lib, pkgs, ... }: {
  home = {
    activation = {
      createControlMasterDirectory =
        lib.hm.dag.entryAfter [ "writeBoundary" ] ''
          run mkdir -p ${config.home.homeDirectory}/.ssh/control-master
        '';
    };
    packages = with pkgs;
      [
        autossh # useful daemon to keep ssh proxies connected
      ];
  };

  programs.ssh = {
    enable = true;

    controlMaster = "auto";
    controlPath = "~/.ssh/control-master/%r@%n:%p";
    controlPersist = "30m";

    hashKnownHosts = true;
    extraOptionOverrides = { VerifyHostKeyDNS = "ask"; };
  };
}
