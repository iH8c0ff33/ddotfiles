{ lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs.stdenv) hostPlatform;
in { services.syncthing.enable = mkIf hostPlatform.isLinux true; }
