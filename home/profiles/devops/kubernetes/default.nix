{ config, lib, pkgs, ... }:
let
  inherit (lib) mkIf optional;
  inherit (pkgs.stdenv) hostPlatform;
in {
  home = {
    packages = with pkgs;
      [ kind kubernetes-helm kubectl kubectx kustomize minikube ]
      ++ optional hostPlatform.isLinux openlens;
  };

  programs.fish.shellAbbrs = mkIf config.programs.fish.enable {
    k = "kubectl";
    ks = "kustomize";
    kns = "kubens";
    kctx = "kubectx";
  };
}
