{ lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs.stdenv) hostPlatform;
in {
  programs.firefox = {
    enable = true;
    package = mkIf hostPlatform.isDarwin pkgs.emptyDirectory;

    profiles = {
      default = {
        id = 0;
        isDefault = true;

        settings = {
          "browser.aboutConfig.showWarning" = false;
          "browser.aboutWelcome.enabled" = false;
          "browser.shell.checkDefaultBrowser" = false;
          "browser.startup.page" = 3;
          "privacy.donottrackheader.enabled" = true;
        };
      };

      proxy = {
        id = 1;

        settings = {
          # This sets proxy type to SOCKS5
          "network.proxy.type" = 1;

          "network.proxy.socks" = "127.0.0.1";
          "network.proxy.socks_port" = 8123;
          "network.proxy.socks_remote_dns" = true;
        };
      };
    };
  };
}
