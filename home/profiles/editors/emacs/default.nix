{ lib, pkgs, ... }:
let
  inherit (lib) mkIf optional;
  inherit (pkgs.stdenv) hostPlatform;
in
{
  home.packages = with pkgs; [
    nodePackages.dockerfile-language-server-nodejs
    sqlite
    # :lang org
    graphviz
  ] ++ optional hostPlatform.isLinux python-language-server;
  programs.emacs = {
    enable = true;
    package = mkIf hostPlatform.isLinux pkgs.emacsUnstable;
    extraPackages = epkgs: with epkgs; [ vterm ];
  };
  services.emacs = mkIf hostPlatform.isLinux {
    enable = true;
    client.enable = true;
  };
}
