{ pkgs, ... }:
{
  home.packages = with pkgs; [
    (texlive.combine {
      inherit (texlive)
        scheme-basic booktabs capt-of catchfile circuitikz
        collection-langitalian dvipng dvisvgm etoolbox footnotehyper
        koma-script metafont pgf preview siunitx standalone svg transparent
        trimspaces ulem unicode-math wrapfig xcolor xetex xkeyval;
    })
    librsvg
  ];
}
