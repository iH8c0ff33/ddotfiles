{ pkgs, ... }: {
  imports = [ ../latex ];
  home.packages = with pkgs; [ pandoc ];
}
