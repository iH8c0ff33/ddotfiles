{ lib, pkgs, ... }:
let
  inherit (lib) mkIf;
  inherit (pkgs.stdenv.hostPlatform) isDarwin;
in {
  imports = [ ../common ];

  programs.fish = {
    enable = true;

    functions = {
      homeup.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)

        set platform (uname -m)-(uname -s | string lower)
        test $platform = "arm64-darwin" ;and set platform "aarch64-darwin"

        set user (id -u -n)

        set tmpdir (mktemp -d)

        nix build -L -o $tmpdir/result $path#homeManagerConfigurations.$platform.$user.activationPackage \
          && sudo sed -i -e "s/nix-env -i/nix profile install/" $tmpdir/result/activate \
          && nix profile remove (nix profile list | grep home-manager-path | cut -d " " -f 4) \
          && $tmpdir/result/activate

        rm -rf $tmpdir
      '';

      nix.body = ''
        set -f prev $_nix_shell_derivations

        if test $argv[1] = shell
          for arg in $argv[2..]
            string match -q "*#*" $arg &&
              set -xa _nix_shell_derivations (string split "#" $arg)[2]
          end
        end

        command nix $argv
        set -f __status $status

        if string length -q $prev
          set _nix_shell_derivations $prev
        else if set -q _nix_shell_derivations
          set -e _nix_shell_derivations
        end

        return $status
      '';

      packup.body = ''
        set repo_dir "$argv[1]"
        test -z "$repo_dir" && set repo_dir "$HOME/dev"

        for dir in $repo_dir/**/.git
          set repo_relpath (realpath --relative-to=$repo_dir (dirname $dir))
          echo -n "$repo_relpath is "
          if [ -z (git -C (dirname $dir) status --porcelain | string collect) ]
            echo (set_color green)"clean"(set_color normal)
          else
            echo (set_color yellow)"dirty"(set_color normal)

            git -C (dirname $dir) status -sb

            read -P "Do you want to stash and push all changes (y/[n])? " -n 1 -l answer
            test -z "$answer" && set answer "n"

            [ $answer = "y" ] \
              && stashadd (dirname $dir) \
              && stash (dirname $dir) \
              && stashpush (dirname $dir)
          end
        end
      '';
      stash.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)

        [ -z (git -C $path status --porcelain | string collect) ] \
          || git -C $path stash --all
      '';

      stashadd.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)

        while test -z "$stashes_repo"
          read -P "Enter stashes repo URL: " -U stashes_repo
        end

        contains stash (git -C $path remote) || git -C $path remote add stash $stashes_repo
      '';

      stashdrop.body = ''
        set hash "$argv[1]"
        test -z "$hash" && echo (set_color red)"error"(set_color normal)": no hash supplied!" && return 1
        set path "$argv[2]"
        test -z "$path" && set path (pwd)
        set name "$argv[3]"
        test -z "$name" && set name (string replace "." "_" (realpath --relative-to=$HOME $path))

        stashadd $path

        set short (git -C $path rev-parse --short $hash)
        test -z "$short" && return 1

        git -C $path push stash :refs/stashes/$name/$short
        git -C $path update-ref -d refs/stashes/$short
      '';

      stashls.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)
        set name "$argv[2]"
        test -z "$name" && set name (string replace "." "_" (realpath --relative-to=$HOME $path))

        stashadd $path

        echo "Local stashes:"
        git -C $path for-each-ref --format="%(objectname)"\t"%(refname)" "refs/stashes/*"
        echo "Remote stashes:"
        git -C $path ls-remote stash "refs/stashes/$name/*"
      '';

      stashpull.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)
        set name "$argv[2]"
        test -z "$name" && set name (string replace "." "_" (realpath --relative-to=$HOME $path))

        stashadd $path

        git -C $path fetch stash "refs/stashes/$name/*:refs/stashes/*"

        for stash in (git -C $path for-each-ref --format="%(objectname)" "refs/stashes/*")
          git -C $path stash store --message (git show --no-patch --format=format:%s $stash) $stash
        end
      '';

      stashpush.body = ''
        set path "$argv[1]"
        test -z "$path" && set path (pwd)
        set name "$argv[2]"
        test -z "$name" && set name (string replace "." "_" (realpath --relative-to=$HOME $path))

        stashadd $path

        git -C $path rev-list --walk-reflogs stash > /dev/null || return 1

        for stash in (git -C $path rev-list --walk-reflogs stash)
          git -C $path push stash $stash:refs/stashes/$name/(git rev-parse --short $stash)
        end
      '';
    };

    loginShellInit = mkIf isDarwin ''
      fish_add_path --move --prepend --path $HOME/.nix-profile/bin /etc/profiles/per-user/daniele/bin /run/current-system/sw/bin /nix/var/nix/profiles/default/bin
    '';

    plugins = with pkgs.fishPlugins; [
      {
        name = "foreign-env";
        inherit (foreign-env) src;
      }
      {
        name = "forgit";
        inherit (forgit) src;
      }
      {
        name = "fzf-fish";
        inherit (fzf-fish) src;
      }
      {
        name = "tide";
        inherit (tide) src;
      }
      {
        name = "z";
        src = pkgs.fetchFromGitHub {
          owner = "jethrokuan";
          repo = "z";
          rev = "85f863f20f24faf675827fb00f3a4e15c7838d76";
          sha256 = "1kaa0k9d535jnvy8vnyxd869jgs0ky6yg55ac1mxcxm8n0rh2mgq";
        };
      }
      {
        name = "fisher";
        src = pkgs.fetchFromGitHub {
          owner = "jorgebucaran";
          repo = "fisher";
          rev = "4.3.2";
          sha256 = "0h9ylnlnvq8vrqa7d2m5mq28jinsm9i253anx8yi3k1n357r6mh5";
        };
      }
    ];

    shellAliases = {
      cat = "${pkgs.bat}/bin/bat"; # pretty (colorful) cat

      # Hi computer! I'm a human.
      df = "df -h";
      du = "du -h";

      ls = "eza"; # pretty ls (same as above)
      l = "ls -lg --git";
      la = "l -a"; # for when I want to see EVERYTHING!
      t = "l -T"; # I like trees
      ta = "l -Ta"; # I like more trees with all the leaves...
    };
  };

  programs.atuin.enable = true;
  programs.fzf.enable = true;
}
