{ lib, pkgs, ... }:
let
  inherit (lib) mkDefault optionals;
  inherit (pkgs.stdenv) hostPlatform;
in
{
  home = {
    sessionVariables = {
      BAT_THEME = "ansi";
      BAT_PAGER = "less";
      PAGER = "less";
      LESS = "-iFMRWX -z-4 -x4";
      LESSOPEN = "|${pkgs.lesspipe}/bin/lesspipe.sh %s";
      EDITOR = mkDefault "vim";
      VISUAL = mkDefault "vim";
    };

    packages = with pkgs; [
      bat
      eza
      file
      less
      lsof
      procs
      vim
    ] ++ optionals hostPlatform.isLinux [ dstat ncdu ];
  };
}
