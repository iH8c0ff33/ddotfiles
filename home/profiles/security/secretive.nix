{ config, ... }: {
  programs.ssh.matchBlocks."*".extraOptions.IdentityAgent =
    "${config.home.homeDirectory}/Library/Containers/com.maxgoedjen.Secretive.SecretAgent/Data/socket.ssh";
}
