{ lib, ... }:
let inherit (lib) mkOption types;
in {
  options.identity = {
    name = mkOption {
      type = with types; uniq str;
      description = "User full name";
    };
    email = mkOption {
      type = with types; uniq str;
      description = "User e-mail address";
    };
    pgpPublicKey = mkOption {
      type = with types; nullOr str;
      description = "User GPG public key";
    };
    sshPublicKeys = mkOption {
      type = with types; listOf str;
      default = [ ];
      description = "User's SSH public keys";
    };
  };
}
