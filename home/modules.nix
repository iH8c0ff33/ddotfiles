{ inputs, cell }:
let inherit (inputs.haumea.lib) load loaders;
in load {
  src = ./modules;
  loader = loaders.verbatim;
}
