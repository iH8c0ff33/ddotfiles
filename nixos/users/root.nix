{ ... }: {
  users.users.root.hashedPassword = "$6$4QgX0Wti.6G2Zqe$ZIK2CHsnUcIi.Z4rpMKejQogOYFT4EFwjLB3XP2YrDb0BHu2u6F6TuAWSUgfBu9C8P5LTkaEIsLecWyX3IZAF/";
}
