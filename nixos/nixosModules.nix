{ inputs, cell }:
let
  inherit (inputs.haumea.lib) load;
  inherit (inputs.cells.haumea.lib.loaders) sensible;
in load {
  inputs = { inherit inputs cell; };
  src = ./modules;
  loader = sensible;
}

