{ inputs, cell }:
let
  inherit (cell) profiles;
  inherit (inputs.cells) common;
in {
  base = common.suites.base
    ++ (with profiles; [ core.boot core.locale nix sudo ]);
}
