{ inputs, cell }:
{ config, ... }:
let
  inherit (builtins) hasAttr isAttrs;
  inherit (inputs.nixpkgs.lib)
    getAttrFromPath mkOption mkOptionType setAttrByPath showFiles showOption
    types;

  erase = optionName: instruction:
    { options, ... }:
    let opt = getAttrFromPath optionName options;
    in {
      options = setAttrByPath optionName (mkOption { visible = false; });
      config.bee._alerts = [{
        assertion = !opt.isDefined;
        message = ''
          The option `${showOption optionName}' is not supported.
            Location: ${showFiles opt.files}
            ${if instruction != null then instruction else ""}
        '';
      }];
    };
in {
  imports = [
    (erase [ "nixpkgs" "config" ]
      "Please set 'config.bee.pkgs' to a fully configured nixpkgs.")
    (erase [ "nixpkgs" "overlays" ]
      "Please set 'config.bee.pkgs' to a nixpkgs - overlays included.")
    (erase [ "nixpkgs" "system" ] "Please set 'config.bee.system', instead.")
    (erase [ "nixpkgs" "localSystem" ]
      "Please set 'config.bee.system', instead.")
    (erase [ "nixpkgs" "crossSystem" ]
      "Please set 'config.bee.system', instead.")
    (erase [ "nixpkgs" "pkgs" ]
      "Please set 'config.bee.pkgs' to an instantiated version of nixpkgs.")
  ];
  options.bee = {
    _alerts = mkOption {
      type = types.listOf types.unspecified;
      internal = true;
      default = [ ];
    };
    _evaled = mkOption {
      type = types.attrs;
      internal = true;
      default = { };
    };
    _unchecked = mkOption {
      type = types.attrs;
      internal = true;
      default = { };
    };
    system = mkOption {
      type = types.str;
      description =
        "divnix/hive requires you to set the host's system via 'config.bee.system = \"x86_64-linux\";'";
    };
    home = mkOption {
      type = mkOptionType {
        name = "input";
        description = "home-manager input";
        check = x: (isAttrs x) && (hasAttr "sourceInfo" x);
      };
      description =
        "divnix/hive requires you to set the home-manager input via 'config.bee.home = inputs.home-22-05;'";
    };
    wsl = mkOption {
      type = mkOptionType {
        name = "input";
        description = "nixos-wsl input";
        check = x: (isAttrs x) && (hasAttr "sourceInfo" x);
      };
      description =
        "divnix/hive requires you to set the nixos-wsl input via 'config.bee.wsl = inputs.nixos-wsl;'";
    };
    darwin = mkOption {
      type = mkOptionType {
        name = "input";
        description = "darwin input";
        check = x: (isAttrs x) && (hasAttr "sourceInfo" x);
      };
      description =
        "divnix/hive requires you to set the darwin input via 'config.bee.darwin = inputs.darwin;'";
    };
    pkgs = mkOption {
      type = mkOptionType {
        name = "packages";
        description = "instance of nixpkgs";
        check = x: (isAttrs x) && (hasAttr "path" x);
      };
      description =
        "divnix/hive requires you to set the nixpkgs instance via 'config.bee.pkgs = inputs.nixos-22.05.legacyPackages;'";
      apply = x:
        if (hasAttr "${config.bee.system}" x) then
          x.${config.bee.system}
        else
          x;
    };
  };
}

