{ inputs, cell }:
let
  inherit (inputs.cells.common) pubkeys;
  inherit (inputs.hive.bootstrap.profiles) bootstrap;
in {
  imports = [ bootstrap ];

  users.users.nixos.openssh.authorizedKeys.keys = pubkeys.ssh.users.daniele;
}
