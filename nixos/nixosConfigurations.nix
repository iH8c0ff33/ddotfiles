{ inputs, cell }:
let
  inherit (inputs.haumea.lib) load;
  inherit (inputs.cells.haumea.lib) loaders transformers;
in load {
  inputs = { inherit inputs cell; };
  src = ./hosts;
  loader = loaders.sensible;
  transformer = transformers.liftDefaultOnly;
}
