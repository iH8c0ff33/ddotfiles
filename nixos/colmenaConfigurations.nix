{ inputs, cell }:
let inherit (inputs.nixpkgs.lib) mapAttrs recursiveUpdate;
in mapAttrs (name: value: {
  imports = [ value ];
  deployment = {
    targetHost = name;
    targetPort = 22;
    targetUser = "daniele";
    sshOptions = [ "-o" "ControlMaster=no" "-o" "ControlPath=none" "-A" ];
  };
}) cell.nixosConfigurations
