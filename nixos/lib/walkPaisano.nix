{ inputs, cell }:
let
  inherit (builtins) intersectAttrs listToAttrs mapAttrs;
  inherit (inputs.nixpkgs.lib)
    attrByPath collect genAttrs nameValuePair pipe systems;

  walkPaisano = self: cellBlock: ops: namer:
    pipe (mapAttrs (system:
      mapAttrs (cell: blocks:
        (pipe blocks ([ (attrByPath [ cellBlock ] { }) ] ++ (ops system cell)
          ++ [ (mapAttrs (target: nameValuePair (namer cell target))) ]))))
      (intersectAttrs (genAttrs systems.doubles.all (_: null)) self)) [
        (collect (x: x ? name && x ? value))
        listToAttrs
      ];
in walkPaisano
