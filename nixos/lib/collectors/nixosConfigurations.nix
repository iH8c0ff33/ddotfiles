{ inputs, cell }:
renamer:
let

  inherit (builtins) mapAttrs;
  inherit (cell.lib) checks transformers walkPaisano;
  inherit (inputs.nixpkgs.lib) filterAttrs;

  cellBlock = "nixosConfigurations";

  walk = self:
    walkPaisano self cellBlock (system: cell: [
      (mapAttrs (target: config: {
        _file = "Cell: ${cell} - Block: ${cellBlock} - Target: ${target}";
        imports = [ config ];
      }))
      (mapAttrs (_: checks.bee))
      (mapAttrs (_: transformers.nixosConfigurations))
      (filterAttrs (_: config: config.bee.system == system))
      (mapAttrs (_: config: config.bee._evaled))
    ]) renamer;
in walk
