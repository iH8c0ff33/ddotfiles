{ inputs, cell }:
renamer:
let
  inherit (builtins) elem mapAttrs;
  inherit (inputs.nixpkgs.lib) filterAttrs fix;
  inherit (cell.lib) checks transformers walkPaisano;

  cellBlock = "colmenaConfigurations";

  walk = self:
    walkPaisano self cellBlock (system: cell: [
      (mapAttrs (target: config: {
        _file = "Cell: ${cell} - Block: ${cellBlock} - Target: ${target}";
        imports = [ config ];
        _module.args.name = renamer cell target;
      }))
      (mapAttrs (_: checks.bee))
      (mapAttrs (_: transformers.colmenaConfigurations))
      (filterAttrs (_: config: config.bee.system == system))
    ]) renamer;

  colmenaTopLevelCliSchema = comb:
    fix (this: {
      __schema = "v0";

      nodes = mapAttrs (_: c: c.bee._evaled) comb;
      toplevel = mapAttrs (_: v: v.config.system.build.toplevel) this.nodes;
      deploymentConfig = mapAttrs (_: v: v.config.deployment) this.nodes;
      deploymentConfigSelected = names:
        filterAttrs (name: _: elem name names) this.deploymentConfig;
      evalSelected = names:
        filterAttrs (name: _: elem name names) this.toplevel;
      evalSelectedDrvPaths = names:
        mapAttrs (_: v: v.drvPath) (this.evalSelected names);
      metaConfig = {
        name = "divnix/hive";
        inherit (import (inputs.self + /flake.nix)) description;
        machinesFile = null;
        allowApplyAll = false;
      };
      introspect = f:
        f {
          lib = inputs.nixpkgs.lib // builtins;
          pkgs = inputs.nixpkgs.legacyPackages.${builtins.currentSystem};
          nodes = mapAttrs (_: c: c.bee._unchecked) comb;
        };
    });
in self: colmenaTopLevelCliSchema (walk self)

