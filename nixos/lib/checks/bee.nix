{ inputs, cell }:
let
  inherit (builtins) concatStringsSep filter isAttrs;
  # I'm too lazy to find out how to get the nixpkgs input directly with
  # divnix/std. So I just get it from hive's  nixpkgs, which follows it.
  inherit (inputs.hive.inputs.nixpkgs.lib.nixos) evalModules;
  inherit (cell.nixosModules) bee;

  check = locatedConfig:
    let
      checked = evalModules {
        modules = [ locatedConfig bee { config._module.check = false; } ];
      };

      failedAsserts = map (x: x.message)
        (filter (x: !x.assertion) checked.config.bee._alerts);

      asserted = if failedAsserts != [ ] then
        throw ''

          Hive's layer sanitation boundary:
          ${concatStringsSep "\n" (map (x: "- ${x}") failedAsserts)}''
      else
        checked;
    in assert isAttrs asserted; {
      inherit locatedConfig;
      evaled = asserted;
    };
in check
