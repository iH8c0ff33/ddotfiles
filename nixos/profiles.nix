{ inputs, cell }:
let
  inherit (inputs.cells.haumea.lib.transformers) liftDefaultOnly;
  inherit (inputs.cells.haumea.lib.loaders) sensible;
  inherit (inputs.haumea.lib) load;
in load {
  inputs = { inherit inputs cell; };
  src = ./profiles;
  transformer = liftDefaultOnly;
  loader = sensible;
}
