{ inputs, cell }:
let
  inherit (inputs.disko.nixosModules) disko;
  inherit (inputs.haumea.lib) load;
  inherit (inputs.cells.common.lib) extendModule;
  inherit (inputs.cells.haumea.lib) loaders transformers;
in load {
  inputs = { inherit inputs cell; };
  src = ./disks;
  loader = inputs: path: extendModule (loaders.sensible inputs path) [ disko ];
  transformer = transformers.liftDefaultOnly;
}

