{ modulesPath, ... }: {
  imports = [ (modulesPath + "/installer/scan/not-detected.nix") ];

  boot = {
    initrd = {
      availableKernelModules =
        [ "nvme" "xhci_pci" "ahci" "usb_storage" "usbhid" "sd_mod" ];
      kernelModules = [ "vfio_pci" "vfio" "vfio_iommu_type1" ];
    };

    kernelModules = [ "kvm-amd" ];
    kernelParams = [
      "vfio-pci.ids=10de:2204,10de:1aef" # bind RTX 3090 to vfio_pci
      # Keychron K2 keyboard Fn Fix
      # NOTE: This needs to be set as a kernel parameter since the hid_apple module
      # is enabled early on boot and modprobe.d options are not applied afterwards
      # if the module is not reinserted manually.
      "hid_apple.fnmode=0"
    ];
    extraModulePackages = [ ];
  };

  fileSystems."/" = {
    device = "/dev/disk/by-uuid/d14c51ed-170f-4fa6-afb1-687665c58e10";
    fsType = "btrfs";
    options = [ "subvol=nixos/root" ];
  };

  fileSystems."/home" = {
    device = "/dev/disk/by-uuid/d14c51ed-170f-4fa6-afb1-687665c58e10";
    fsType = "btrfs";
    options = [ "subvol=nixos/home" ];
  };

  fileSystems."/nix" = {
    device = "/dev/disk/by-uuid/d14c51ed-170f-4fa6-afb1-687665c58e10";
    fsType = "btrfs";
    options = [ "subvol=nixos/nix" ];
  };

  fileSystems."/boot" = {
    device = "/dev/disk/by-uuid/B32E-E9B1";
    fsType = "vfat";
  };
}
