{ inputs, cell }:
let inherit (inputs.nixos-hardware.nixosModules) common-cpu-intel;
in { config, modulesPath, ... }: {
  imports =
    [ common-cpu-intel (modulesPath + "/installer/scan/not-detected.nix") ];

  boot = {
    initrd = {
      availableKernelModules =
        [ "xhci_pci" "thunderbolt" "nvme" "usb_storage" "usbhid" "sd_mod" ];
      kernelModules = [ ];
    };
    extraModulePackages = [ ];
    kernelModules = [ "kvm_intel" ];
    # Use last ZFS-compatible kernel
    kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
    kernelParams = [ "i915.enable_guc=3" ];
  };

  # TODO: Enable microcode updates
}

