{ inputs, cell }:
let inherit (inputs.nixos-hardware.nixosModules) common-cpu-intel;
in { modulesPath, ... }: {
  imports =
    [ common-cpu-intel (modulesPath + "/installer/scan/not-detected.nix") ];

  boot = {
    initrd = {
      availableKernelModules =
        [ "xhci_pci" "ahci" "usb_storage" "sd_mod" "sdhci_pci" ];
      kernelModules = [ ];
    };
    kernelModules = [ "kvm-intel" ];
    extraModulePackages = [ ];
  };
}
