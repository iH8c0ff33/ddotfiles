{ config, pkgs, ... }: {
  boot = {
    initrd = {
      availableKernelModules =
        [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
      kernelModules = [ ];
    };
    extraModulePackages = [ ];
    kernelModules = [ "kvm-amd" ];
    kernelPackages = pkgs.linuxPackages_latest;
  };

  hardware = {
    enableRedistributableFirmware = true;

    nvidia.modesetting.enable = true;

    opengl = {
      enable = true;
      driSupport32Bit = true;
    };
  };

  services.xserver.displayManager.gdm.wayland = false;
  services.xserver.videoDrivers = [ "nvidia" ];
}
