{ inputs, cell }:
let
  inherit (builtins) mapAttrs;
  inherit (inputs.cells.common) pubkeys;
  inherit (inputs.nixos-generators) nixosGenerate;
  inherit (cell) generators;
in (mapAttrs (_: generator:
  nixosGenerate {
    inherit (inputs.nixpkgs) system;
    modules = [ generator ];
    format = "iso";
  }) generators) // {
    rpi-sd-image = nixosGenerate {
      system = "aarch64-linux";
      modules = [{
        boot.kernelParams = [ "8250.nr_uarts=1" ];
        users.users.nixos.openssh.authorizedKeys.keys =
          pubkeys.ssh.users.daniele;
      }];
      format = "sd-aarch64-installer";
    };
  } // {
    inherit (inputs.colmena.packages) colmena;
  }

