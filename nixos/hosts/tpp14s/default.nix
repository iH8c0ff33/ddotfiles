# Edit this configuration file to define what should be installed on
# your system.  Help is available in the configuration.nix(5) man page
# and in the NixOS manual (accessible by running ‘nixos-help’).

{ inputs, cell }:
let
  inherit (inputs.cells.common) channels profiles suites;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
  homeSuites = inputs.cells.home.suites;
  nixosSuites = cell.suites;
in { pkgs, ... }:

{
  # imports = [
  #   # Include the results of the hardware scan.
  #   ../../profiles/laptop
  #   ../../profiles/graphical/lightdm
  #   ../../profiles/graphical/sway
  #   ../../profiles/sound/pipewire
  # ];

  imports = [
    (mkBee {
      system = "x86_64-linux";
      channel = channels.unstable;
    })
    ./hardware-configuration.nix
    (mkUserModule "daniele" homeSuites.laptop)

  ] ++ nixosSuites.base ++ (with suites; virtualisation) ++ (with profiles; [
    laptop
    graphical.plasma
    misc.fwupd
    misc.kanata
    network.bluetooth
    network.mdns
    network.zigbee
    security.fingerprint
  ]);

  boot = {
    binfmt.emulatedSystems = [ "aarch64-linux" ];
    # Newest kernel is required for the AMD integrated GPU
    kernelPackages = pkgs.linuxPackages_latest;

    loader = {
      # Use the systemd-boot EFI boot loader.
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  networking = {
    hostName = "tpp14s";
    networkmanager.enable = true;
  };

  environment.systemPackages = [ pkgs.networkmanager-l2tp ];

  services.tailscale.enable = true;

  systemd.tmpfiles.rules =
    [ "L /etc/ipsec.secrets - - - - /etc/ipsec.d/ipsec.nm-l2tp.secrets" ];
  system.stateVersion = "24.11";
}
