{ inputs, cell }:
let
  inherit (inputs.cells.common) profiles users;
  inherit (inputs.cells.common.lib) mkBee;
in { config, ... }: {
  imports = [
    (mkBee { system = "aarch64-linux"; })
    ./hardware-configuration.nix
    # Import only the user (and ssh keys)
    profiles.network.ssh
    users.daniele
  ];

  boot.loader = {
    grub.enable = false;
    generic-extlinux-compatible.enable = true;
  };

  networking = {
    hostName = "octopi";
    networkmanager.enable = true;
    firewall = {
      allowedTCPPorts = [ 80 443 10250 ];
      allowedUDPPorts = [ 8472 ];
    };
  };

  nix = {
    buildMachines = [{
      hostName = "workstation.priv.deutron.ml";
      sshKey = "/etc/nix/id_octopi_ed25519";
      systems = [ "x86_64-linux" "aarch64-linux" ];
      maxJobs = 4;
      speedFactor = 2;
      supportedFeatures = [ "nixos-test" "benchmark" "big-parallel" "kvm" ];
    }];

    distributedBuilds = true;

    extraOptions = ''
      builders-use-substitutes = true
    '';
  };

  services.k3s = {
    enable = true;
    role = "agent";
    serverAddr = "https://10.0.0.108:6443";
    tokenFile = config.age.secrets."pik3s_node-token".file;
  };

  systemd.network.links."10-flannel" = {
    matchConfig."OriginalName" = "flannel*";
    linkConfig."MACAddressPolicy" = "none";
  };
}
