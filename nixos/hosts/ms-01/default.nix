{ inputs, cell }:
let
  inherit (cell) diskoConfigurations hardwareConfigurations suites;
  inherit (inputs.cells.common) channels profiles;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
  homeProfiles = inputs.cells.home.profiles;
  nixosProfiles = cell.profiles;
in { config, lib, pkgs, ... }: {
  imports = [
    (mkBee {
      system = "x86_64-linux";
      channel = channels.unstable;
    })
    diskoConfigurations.ms-01
    hardwareConfigurations.ms-01
    (mkUserModule "daniele" [ homeProfiles.network.syncthing ])
  ] ++ suites.base ++ (with profiles; [
    misc.fwupd
    network.ssh
    nixosProfiles.filesystems.zfs
  ]);

  boot.zfs.extraPools = [ "data" ];

  networking = {
    hostName = "ms-01";
    hostId = "db4e152e";
  };

  security.sudo.wheelNeedsPassword = false;

  services.tailscale.enable = true;

  system.stateVersion = "24.05";
}
