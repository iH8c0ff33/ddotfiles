{ inputs, cell }:
let
  inherit (cell) diskoConfigurations hardwareConfigurations;
  inherit (inputs.cells.common) profiles;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
in { pkgs, ... }:

{
  imports = [
    (mkBee { system = "x86_64-linux"; })
    diskoConfigurations.nuc
    hardwareConfigurations.nuc
    (mkUserModule "daniele" [ ])
  ] ++ (with profiles; [ misc.fwupd network.ssh ]);

  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
  };

  networking = {
    hostName = "nuc";
    hostId = "a266f838";
  };

  services.tailscale.enable = true;
}
