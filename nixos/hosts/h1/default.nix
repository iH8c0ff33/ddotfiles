{ inputs, cell }:
let
  inherit (cell) diskoConfigurations hardwareConfigurations;
  inherit (inputs.cells.common) channels profiles suites;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
  homeSuites = inputs.cells.home.suites;
in { config, pkgs, ... }: {
  imports = cell.suites.base ++ [
    (mkBee {
      system = "x86_64-linux";
      channel = channels.unstable;
      config = {
        allowUnfree = true;
        nvidia.acceptLicense = true;
      };
    })
    diskoConfigurations.h1
    hardwareConfigurations.h1
    (mkUserModule "daniele" homeSuites.desktop)
    (mkUserModule "sara" homeSuites.desktop)
  ] ++ (with suites; virtualisation ++ gnome)
    ++ (with profiles; [ network.bluetooth network.ssh shell.fish ]);

  networking.hostName = "h1";

  security.pam.sshAgentAuth.enable = true;

  ## Tailscale
  services.tailscale.enable = true;

  system.stateVersion = "24.05";
}
