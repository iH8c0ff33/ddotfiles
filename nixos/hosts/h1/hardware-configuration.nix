# Do not modify this file!  It was generated by ‘nixos-generate-config’
# and may be overwritten by future invocations.  Please make changes
# to /etc/nixos/configuration.nix instead.
{ config, lib, pkgs, modulesPath, ... }:

{
  imports =
    [ (modulesPath + "/installer/scan/not-detected.nix")
    ];

  boot.initrd.availableKernelModules = [ "nvme" "xhci_pci" "ahci" "usbhid" "usb_storage" "sd_mod" ];
  boot.initrd.kernelModules = [ ];
  boot.kernelModules = [ "kvm-amd" ];
  # boot.kernelPackages = pkgs.linuxPackages;
  boot.extraModulePackages = [ ];
  boot.supportedFilesystems = [ "btrfs" ];

  fileSystems."/" =
    { device = "/dev/disk/by-uuid/68bf84be-7b96-4865-8659-587fccac8731";
      fsType = "btrfs";
      options = [ "subvol=nixos/root" "noatime" ];
    };

  fileSystems."/home" =
    { device = "/dev/disk/by-uuid/68bf84be-7b96-4865-8659-587fccac8731";
      fsType = "btrfs";
      options = [ "subvol=nixos/home" "noatime" ];
    };

  fileSystems."/boot" =
    { device = "/dev/disk/by-uuid/7D36-DBE9";
      fsType = "vfat";
    };

  fileSystems."/nix" =
    { device = "/dev/disk/by-uuid/68bf84be-7b96-4865-8659-587fccac8731";
      fsType = "btrfs";
      options = [ "subvol=nixos/nix" "compress=zstd" "noatime" ];
    };

  swapDevices =
    [ { device = "/dev/disk/by-uuid/94b4a7a1-48de-4d47-82c6-d43817390d50"; }
    ];

  # Enables DHCP on each ethernet and wireless interface. In case of scripted networking
  # (the default) this is the recommended approach. When using systemd-networkd it's
  # still possible to use this option, but it's recommended to use it in conjunction
  # with explicit per-interface declarations with `networking.interfaces.<interface>.useDHCP`.
  networking.useDHCP = lib.mkDefault true;
  # networking.interfaces.enp4s0.useDHCP = lib.mkDefault true;
  # networking.interfaces.wlp5s0.useDHCP = lib.mkDefault true;

  nixpkgs.hostPlatform = lib.mkDefault "x86_64-linux";
  hardware.cpu.amd.updateMicrocode = lib.mkDefault config.hardware.enableRedistributableFirmware;
}
