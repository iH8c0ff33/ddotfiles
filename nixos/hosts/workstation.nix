{ inputs, cell }:
let
  inherit (cell) hardwareConfigurations;
  inherit (inputs.cells.common) channels profiles suites;
  inherit (inputs.cells.common.lib) mkBee mkUserModule;
  homeSuites = inputs.cells.home.suites;
  nixosProfiles = cell.profiles;
  nixosSuites = cell.suites;
in { pkgs, ... }: {
  imports = [
    (mkBee {
      system = "x86_64-linux";
      channel = channels.unstable;
    })
    hardwareConfigurations.workstation
    (mkUserModule "daniele" homeSuites.desktop)
  ] ++ nixosSuites.base ++ (with suites; virtualisation) ++ (with profiles; [
    misc.kanata
    network.ssh
    nixosProfiles.security.lanzaboote
  ]);

  boot = {
    binfmt.emulatedSystems = [ "aarch64-linux" ];
    lanzaboote.settings.default = "@saved";
  };

  networking = {
    hostName = "workstation";
    networkmanager.enable = true;

    firewall = {
      checkReversePath = "loose";
      interfaces."tailscale0".allowedTCPPorts = [ 80 443 6443 25565 ];
      trustedInterfaces = [ "cni0" "flannel.1" ];
    };
  };

  security.pam.sshAgentAuth = {
    enable = true;
    authorizedKeysFiles = [ "/etc/ssh/authorized_keys.d/daniele" ];
  };

  services.xserver.displayManager.gdm.autoSuspend = false;
  services.xserver.desktopManager.plasma5.useQtScaling = true;

  services.k3s.enable = true;

  services.tailscale.enable = true;

  users.users.root.openssh.authorizedKeys.keys = [
    "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIDObm/zwIKa5ZD7Mb9PMg/ykYbzKforpgMELAxxUO0OZ root@octopi.priv.deutron.ml"
  ];

  system.stateVersion = "22.11";
}
