{
  disko.devices = {
    disk.ssd = {
      type = "disk";
      device = "/dev/disk/by-id/ata-KINGSTON_SA400S37240G_50026B76827C2D48";

      content = {
        type = "gpt";
        partitions = {
          boot = {
            start = "1M";
            size = "1G";
            type = "ef00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
              mountOptions = [ "defaults" "fmask=0137" "dmask=0027" ];
            };
          };

          btrfs = {
            size = "100%";
            content = {
              type = "btrfs";
              subvolumes = {
                root = { mountpoint = "/"; };
                home = {
                  mountpoint = "/home";
                  mountOptions = [ "compress=zstd" ];
                };
                nix = {
                  mountpoint = "/nix";
                  mountOptions = [ "compress=zstd" "noatime" ];
                };
              };
            };
          };
        };
      };
    };
  };
}
