{
  disko.devices = {
    disk.ssd = {
      type = "disk";
      device = "/dev/disk/by-id/ata-Samsung_SSD_850_EVO_250GB_S21PNXAG533503H";

      content = {
        type = "gpt";
        partitions = {
          boot = {
            start = "1M";
            size = "1G";
            type = "ef00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
              mountOptions = [ "defaults" "fmask=0137" "dmask=0027" ];
            };
          };

          btrfs = {
            size = "100%";
            content = {
              type = "btrfs";
              subvolumes = {
                root = { mountpoint = "/"; };
                home = {
                  mountpoint = "/home";
                  mountOptions = [ "compress=zstd" ];
                };
                nix = {
                  mountpoint = "/nix";
                  mountOptions = [ "compress=zstd" "noatime" ];
                };
              };
            };
          };
        };
      };
    };
  };
}

