{
  disko.devices = {
    disk.nvme = {
      type = "disk";
      device = "/dev/disk/by-id/nvme-Samsung_SSD_970_EVO_250GB_S465NB0K713330V";

      content = {
        type = "gpt";
        partitions = {
          boot = {
            start = "1M";
            size = "1G";
            type = "ef00";
            content = {
              type = "filesystem";
              format = "vfat";
              mountpoint = "/boot";
              mountOptions = [ "defaults" "fmask=0137" "dmask=0027" ];
            };
          };

          luks = {
            size = "100%";
            content = {
              type = "luks";
              name = "luks";
              settings = {
                allowDiscards = true;
                crypttabExtraOpts = [ "tpm2-device=auto" ];
              };
              content = {
                type = "btrfs";
                subvolumes = {
                  root = {
                    mountpoint = "/";
                    mountOptions = [ "noatime" ];
                  };
                  home = {
                    mountpoint = "/home";
                    mountOptions = [ "compress=zstd" "noatime" ];
                  };
                  nix = {
                    mountpoint = "/nix";
                    mountOptions = [ "compress=zstd" "noatime" ];
                  };
                };
              };
            };
          };
        };
      };
    };
  };
}

