{ inputs, cell }:
let inherit (inputs.lanzaboote.nixosModules) lanzaboote;
in { lib, pkgs, ... }:
let inherit (lib) mkForce;
in {
  imports = [ lanzaboote ];

  boot = {
    bootspec.enable = true;
    initrd.systemd.enable = true;
    loader.systemd-boot.enable = mkForce false;
    lanzaboote = {
      enable = true;
      pkiBundle = "/etc/secureboot";
    };
  };

  environment.systemPackages = [ pkgs.sbctl ];
}
