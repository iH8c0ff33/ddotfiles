{ lib, ... }:
let inherit (lib) mkDefault;
in {
  nix = {
    gc.dates = "weekly";
    optimise.automatic = mkDefault true;
  };
}
