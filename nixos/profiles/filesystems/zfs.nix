{ lib, ... }:
let inherit (lib) mkDefault;
in {
  boot = {
    supportedFilesystems = [ "zfs" ];
    zfs.forceImportRoot = false;
  };
  services.zfs = {
    autoScrub.enable = mkDefault true;
    # This enables the auto-snapshot service only, automatic snapshotting still
    # needs to be enabled on each dataset with the `com.sun:auto-snapshot'
    # property.
    autoSnapshot.enable = mkDefault true;
  };
}
