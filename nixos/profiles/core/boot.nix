{ lib, pkgs, ... }:
let inherit (lib) mkDefault;
in {
  boot.loader = {
    efi.canTouchEfiVariables = mkDefault true;
    systemd-boot.enable = mkDefault true;
  };
  environment.systemPackages = [ pkgs.efibootmgr ];

  powerManagement.cpuFreqGovernor = mkDefault "powersave";

  systemd = {
    services.systemd-udev-settle.enable = false;
    services.NetworkManager-wait-online.enable = false;

    extraConfig = ''
      DefaultTimeoutStopSec=15s
    '';
  };
}
