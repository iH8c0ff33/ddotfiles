{ fetchFromGitHub, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "chiabip158";
  version = "1.0";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = pname;
    rev = version;
    sha256 = "158j77fph09f9wgisznnv1p4jwarx8rrcfcfwfydlirsgjygl6k0";
  };

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  format = "pyproject";

  nativeBuildInputs = with python3Packages; [ pybind11 setuptools-scm ];
}
