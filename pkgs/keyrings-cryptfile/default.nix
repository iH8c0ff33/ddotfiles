{ fetchFromGitHub, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "keyrings.cryptfile";
  version = "1.3.4";

  src = fetchFromGitHub {
    owner = "frispete";
    repo = "keyrings.cryptfile";
    rev = "v${version}";
    sha256 = "11zwfz3ahdx4pzhqh6nlri2r2r3dj5cdcq0zrv9aqff5lghwmzz7";
  };

  # NOTE: disabled tests since 1.3.4 tests doesn't work with the new keyring
  # package which renamed module tests to testing.
  doCheck = false;
  # nativeBuildInputs = with python3Packages; [ pytest ];

  propagatedBuildInputs = with python3Packages; [
    argon2_cffi
    keyring
    pycryptodome
  ];
}
