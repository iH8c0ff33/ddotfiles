{ boost, cmake, fetchFromGitHub, gmp, python3Packages, substituteAll }:
let
  pybind11 = fetchFromGitHub {
    owner = "pybind";
    repo = "pybind11";
    rev = "v2.6.2";
    sha256 = "1lsacpawl2gb5qlh0cawj9swsyfbwhzhwiv6553a7lsigdbadqpy";
  };
in
python3Packages.buildPythonPackage rec {
  pname = "chiavdf";
  version = "1.0.1";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = pname;
    rev = version;
    sha256 = "039qgz27rm15l1mdrj39zs05bbb4ykv62iwyjkx1ykxy625h3jcz";
  };

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  dontUseCmakeConfigure = true;

  format = "pyproject";

  patches = [
    (substituteAll {
      src = ./cmake_no_fetchcontent.patch;
      inherit pybind11;
    })
  ];

  nativeBuildInputs = [ cmake ]
    ++ (with python3Packages; [ setuptools-scm ]);

  propagatedBuildInputs = [ boost gmp ];
}
