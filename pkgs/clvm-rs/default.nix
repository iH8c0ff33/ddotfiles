{ fetchFromGitHub, perl, python3Packages, rustPlatform }:
python3Packages.buildPythonPackage rec {
  pname = "clvm-rs";
  version = "0.1.7";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "clvm_rs";
    rev = version;
    sha256 = "1xb8g3x64gd6zpi90v0f6vk39js68rf9gg82qxg7jhvfmpg3dsxx";
  };

  cargoDeps = rustPlatform.fetchCargoTarball {
    inherit src;
    name = "${pname}-${version}";
    hash = "sha256-32kwJuoWmhNEMqnnGT/qI2hrXROiFZHVdJbozr1R6tk=";
  };

  RUST_BACKTRACE = "full";

  format = "pyproject";

  nativeBuildInputs = [ perl ] ++ (with rustPlatform; [
    cargoSetupHook
    maturinBuildHook
  ]);
}
