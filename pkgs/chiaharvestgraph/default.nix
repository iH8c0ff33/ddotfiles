{ fetchFromGitHub, stdenv }:
stdenv.mkDerivation {
  pname = "chiaharvestgraph";
  version = "0.0.1";

  src = fetchFromGitHub {
    owner = "stolk";
    repo = "chiaharvestgraph";
    rev = "279dfb0890e3845675bc37c0955ab9a3376f2f2b";
    sha256 = "12zcwwjdgla6khxbxzrdn3kw9sn970qznlnl397rh487s2h4k3cr";
  };

  installPhase = ''
    mkdir -p $out/bin
    cp chiaharvestgraph $out/bin/
  '';
}
