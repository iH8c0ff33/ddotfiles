{ blspy, fetchFromGitHub, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "clvm";
  version = "0.9.6";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "clvm";
    rev = version;
    sha256 = "1h1d5fcbcnv815a502q0fy9q7r2z4cz31i6n9wj1v1y5a250852w";
  };

  format = "pyproject";

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  nativeBuildInputs = with python3Packages; [ setuptools-scm ];

  propagatedBuildInputs = [ blspy ];
}
