channels: final: prev: {
  inherit (channels.nixos-unstable)
    # NOTE: emacs.cacahix.org caches emacs builds using nixos-unstable inputs
    emacs
    eza
    fish
    k3s_1_29
    neovim
    poetry
    nix
    nixUnstable;
}
