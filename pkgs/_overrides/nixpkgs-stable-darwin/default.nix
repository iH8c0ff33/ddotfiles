channels: final: prev: let
  inherit (prev.lib) remove;
in rec {
  inherit (channels.nixos-unstable)
    # NOTE: emacs.cacahix.org caches emacs builds using nixos-unstable inputs
    emacs
    fish
    neovim
    nix
    nixUnstable;
}
