{ clvm, fetchFromGitHub, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "clvm-tools";
  version = "0.4.3";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "clvm_tools";
    rev = version;
    sha256 = "122pvj7q7b9sk1mpa8hvsb1ga87vh2rziahkj7zqd4ns59hgfv3d";
  };

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  format = "pyproject";

  nativeBuildInputs = with python3Packages; [ setuptools-scm ];

  propagatedBuildInputs = [ clvm ];
}
