{fetchFromGitHub, python3Packages}:
python3Packages.buildPythonPackage rec {
  pname = "concurrent-log-handler";
  version = "0.9.19";

  src = fetchFromGitHub {
    owner = "Preston-Landers";
    repo = pname;
    rev = version;
    sha256 = "19lrxwdblihm2q1z11fhhn7dq6bj3qqga36z3mp1mdhaf4212k1r";
  };

  propagatedBuildInputs = with python3Packages; [ portalocker ];
}
