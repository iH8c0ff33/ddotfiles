final: prev: {
  desert = prev.callPackage ./desert { };
  blspy = prev.callPackage ./blspy { };
  chiabip158 = prev.callPackage ./chiabip158 { };
  # TODO: broken
  # chia-blockchain = prev.callPackage ./chia-blockchain { };
  chiapos = prev.callPackage ./chiapos { };
  chiavdf = prev.callPackage ./chiavdf { };
  chiaharvestgraph = prev.callPackage ./chiaharvestgraph { };
  clvm = prev.callPackage ./clvm { };
  clvm-rs = prev.callPackage ./clvm-rs { };
  clvm-tools = prev.callPackage ./clvm-tools { };
  concurrent-log-handler = prev.callPackage ./concurrent-log-handler { };
  fish-kubectl-completions = prev.callPackage ./fish-kubectl-completions { };
  greetd = prev.callPackage ./greetd { };
  keyrings-cryptfile = prev.callPackage ./keyrings-cryptfile { };
  madmax-plotter = prev.callPackage ./madmax-plotter { };
  # gnomeExtensions.pop-shell = prev.callPackage ./pop-shell { };
  plotman = prev.callPackage ./plotman { };
  snmpsim = prev.callPackage ./snmpsim { };
}
