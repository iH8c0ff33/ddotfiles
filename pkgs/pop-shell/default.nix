{ fetchFromGitHub, glib, nodePackages, stdenv, ... }:
stdenv.mkDerivation rec {
  pname = "pop-shell";
  version = "59ca9ce";

  src = fetchFromGitHub {
    owner = "pop-os";
    repo = "shell";
    rev = "59ca9cede3c185c347bd2ae3d40882e020fb1fd0";
    sha256 = "14fjnc25glzsra1m8v1fnrr3d5jir3g3lpizk0kc8srzp231qp4n";
  };

  nativeBuildInputs = [ glib nodePackages.typescript ];

  # the gschema doesn't seem to be installed properly (see dconf)
  makeFlags = [
    "INSTALLBASE=$(out)/share/gnome-shell/extensions"
    "PLUGIN_BASE=$(out)/share/pop-shell/launcher"
    "SCRIPTS_BASE=$(out)/share/pop-shell/scripts"
  ];
}
