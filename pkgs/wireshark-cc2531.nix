{ stdenv, fetchFromGitHub }:
stdenv.mkDerivation {
  pname = "wireshark-cc2531";
  version = "0.0.0";

  src = fetchFromGitHub {
    owner = "andrebdo";
    repo = "wireshark-cc2531";
    sha256 = "0a06mlf7d2i0xlpy8w9c0wdcqrkhx6jkzp0fpv05mzp6vvp5j01l";
    rev = "afcc4a40c5e3d160a971b7cb8ee99f12ea31dc3a";
  };

  buildPhase = ''
    chmod +x build.sh
    ./build.sh
  '';

  installPhase = ''
    mkdir -p $out/lib/wireshark/extcap
    install cc2531 $out/lib/wireshark/extcap/cc2531
  '';
}
