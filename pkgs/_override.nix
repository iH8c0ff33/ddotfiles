{ inputs, nixpkgsArgs, ... }: final: prev:
let
  nixos-2105 = import inputs.nixos-2105 nixpkgsArgs;
in
{
  inherit (nixos-2105) weechat;
}
