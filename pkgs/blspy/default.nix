{ cmake, fetchFromGitHub, git, python3Packages, substituteAll }:
let
  pybind11 = fetchFromGitHub {
    owner = "pybind";
    repo = "pybind11";
    rev = "v2.6.2";
    sha256 = "1lsacpawl2gb5qlh0cawj9swsyfbwhzhwiv6553a7lsigdbadqpy";
  };

  relic = fetchFromGitHub {
    owner = "relic-toolkit";
    repo = "relic";
    rev = "1885ae3b681c423c72b65ce1fe70910142cf941c";
    sha256 = "1pdhx3c4nvwiki3xnkg04c03w15jbs0cfpm76rxwp5rcrd6rki5n";
  };
in
python3Packages.buildPythonPackage rec {
  pname = "blspy";
  version = "1.0.2";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "bls-signatures";
    rev = version;
    sha256 = "1z2sj7ksa9x106k7c5mfslxghnxcwd0q8w4lc8k0d76cd9bgh70q";
  };

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  dontUseCmakeConfigure = true;

  patches = [
    (substituteAll {
      src = ./cmake_disable_fetchcontent.patch;
      inherit pybind11 relic;
    })
  ];

  nativeBuildInputs = [ cmake git ] ++ (with python3Packages; [ setuptools_scm ]);
}
