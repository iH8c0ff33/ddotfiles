{ desert, fetchFromGitHub, python3Packages }:
python3Packages.buildPythonApplication rec {
  pname = "plotman";
  version = "0.3.1";

  src = fetchFromGitHub {
    owner = "ericaltendorf";
    repo = "plotman";
    rev = "v${version}";
    sha256 = "0jangyncxvq29mhgp56zb5q2i3v5671jkv0mpmz6cmlbpz0b89jk";
  };

  patches = [ ./fix-dependencies.patch ./fix-nix.patch ];

  propagatedBuildInputs = with python3Packages; [
    appdirs
    attrs
    click
    desert
    (marshmallow.overrideAttrs (oldAttrs: rec {
      version = "3.12.1";
      src = fetchPypi {
        inherit (oldAttrs) pname;
        inherit version;
        sha256 = "0h70m4z1kbcwsd0sv8cwlcmpj4dnblvr5vj18j7wa327f1dlfl40";
      };
    }))
    pendulum
    psutil
    pyyaml
    texttable
  ];
}
