{ fetchFromGitHub, python3Packages }:
python3Packages.buildPythonApplication rec {
  pname = "snmpsim";
  version = "1.0.0-rc.1";

  src = fetchFromGitHub {
    owner = "etingof";
    repo = "snmpsim";
    sha256 = "01342bfn9v3lhad5yvkdhnlm3xp5z6kliv2wwlxnjla1wv6dwmpf";
    rev = "7c1f5bd5fc36bd68dde326f1b1988efcfef93fa9";
  };

  patches = [ ./0001-Fix-typos-in-rec2rec-args.patch ];

  propagatedBuildInputs = with python3Packages; [ pysnmp ];
}
