{ cmake, fetchFromGitHub, python3Packages, substituteAll }:
let
  pybind11 = fetchFromGitHub {
    owner = "pybind";
    repo = "pybind11";
    rev = "v2.6.2";
    sha256 = "1lsacpawl2gb5qlh0cawj9swsyfbwhzhwiv6553a7lsigdbadqpy";
  };

  cxxopts = fetchFromGitHub {
    owner = "jarro2783";
    repo = "cxxopts";
    rev = "v2.2.1";
    sha256 = "0d3y747lsh1wkalc39nxd088rbypxigm991lk3j91zpn56whrpha";
  };

  gulrak = fetchFromGitHub {
    owner = "gulrak";
    repo = "filesystem";
    rev = "v1.5.4";
    sha256 = "0ynhh9rybhms805khjzx85wnda2gai1h6gkss862528dmb659wsa";
  };
in
python3Packages.buildPythonPackage rec {
  pname = "chiapos";
  version = "1.0.2";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "chiapos";
    rev = version;
    sha256 = "1lmdm0jqzpkjb1z74qq0m9l2wfg478gm54ch8lra9g5ivwymb4hc";
  };

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  dontUseCmakeConfigure = true;

  patches = [
    (substituteAll {
      src = ./cmake_disable_fetchcontent.patch;
      inherit pybind11 cxxopts gulrak;
    })
  ];

  nativeBuildInputs = [ cmake pybind11 ]
    ++ (with python3Packages; [ setuptools_scm ]);

  buildInputs = with python3Packages; [ psutil ];
}
