# Stolen from https://github.com/bqv/nixrc/blob/live/pkgs/applications/display-managers/greetd/tuigreet.nix

{ naersk, fetchgit, ... }:

naersk.buildPackage rec {
  name = "greetd";
  version = "0.2.0";

  src = fetchgit {
    url = "https://github.com/apognu/tuigreet";
    rev = version;
    sha256 = "1fk8ppxr3a8vdp7g18pp3sgr8b8s11j30mcqpdap4ai14v19idh8";
  };

  buildInputs = [
  ];
}

