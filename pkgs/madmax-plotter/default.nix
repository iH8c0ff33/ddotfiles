{ cmake
, fetchFromGitHub
, git
, gmp
, libsodium
, pythonPackages
, python
, stdenv
, substituteAll
, ...
}:
let
  relic = fetchFromGitHub {
    owner = "relic-toolkit";
    repo = "relic";
    rev = "e6209fd80e07203b865983faee635fa1f85d6c9f";
    sha256 = "03h42n27gc0k9qpvrgf2pwkwsi3a8j2sn1anlvrh07f2i4znjbm6";
  };
in
stdenv.mkDerivation rec {
  pname = "madmax-plotter";
  version = "974d6e5";

  src = fetchFromGitHub {
    owner = "madMAx43v3r";
    repo = "chia-plotter";
    rev = "974d6e5f1440f68c48492122ca33828a98864dfc";
    sha256 = "0dliswvqmi3wq9w8jp0sb0z74n5k37608sig6r60z206g2bwhjja";

    fetchSubmodules = true;
  };

  patches = [
    (substituteAll {
      src = ./cmake_disable_fetchcontent.patch;
      pybind11_src = pythonPackages.pybind11.src;
      relic_src = relic;
    })
    ./relic_include_dir_fix.patch
    ./bls_library_rename_fix.patch
    ./cmake_install_fix.patch
  ];

  nativeBuildInputs = [ cmake git python ];

  buildInputs = [ libsodium gmp.static ];
}
