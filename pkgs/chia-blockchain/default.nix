{ blspy
, chiabip158
, chiapos
, chiavdf
, clvm
, clvm-rs
, clvm-tools
  , concurrent-log-handler
, fetchFromGitHub
, fetchpatch
, keyrings-cryptfile
, python3Packages
}:
python3Packages.buildPythonApplication rec {
  pname = "chia-blockchain";
  version = "1.1.6";

  src = fetchFromGitHub {
    owner = "Chia-Network";
    repo = "chia-blockchain";
    rev = version;
    sha256 = "12z4zzfyz1d4qkz06b7vgd8riii498pbax2mq2j2lc6sakghm1z2";
  };

  format = "pyproject";

  SETUPTOOLS_SCM_PRETEND_VERSION = version;

  nativeBuildInputs = with python3Packages; [ setuptools_scm ];

  propagatedBuildInputs = [ blspy chiapos keyrings-cryptfile ]
    ++ (with python3Packages; [
    ((aiohttp.overrideAttrs (oldAttrs: rec {
      version = "3.7.4";
      src = python3Packages.fetchPypi {
        inherit (oldAttrs) pname;
        inherit version;
        sha256 = "1pn79h8fng4xi5gl1f6saw31nxgmgyxl41yf3vba1l21673yr12x";
      };
    })).override {
      chardet = chardet.overrideAttrs (oldAttrs: rec {
        version = "3.0.4";

        src = python3Packages.fetchPypi {
          inherit (oldAttrs) pname;
          inherit version;
          sha256 = "1bpalpia6r5x1kknbk11p1fzph56fmmnp405ds8icksd3knr5aw4";
        };

        patches = [
          # Add pytest 4 support. See: https://github.com/chardet/chardet/pull/174
          (fetchpatch {
            url = "https://github.com/chardet/chardet/commit/0561ddcedcd12ea1f98b7ddedb93686ed8a5ffa4.patch";
            sha256 = "1y1xhjf32rdhq9sfz58pghwv794f3w2f2qcn8p6hp4pc8jsdrn2q";
          })
        ];

        checkInputs = [ pytest pytestrunner hypothesis ];
      });
    })
    aiosqlite
    chiabip158
    chiavdf
    bitstring
    click
    clvm
    clvm-rs
    clvm-tools
    colorlog
    concurrent-log-handler
    cryptography
    dnspython
    importlib-metadata
    (keyring.overrideAttrs (oldAttrs: rec {
      version = "23.0.1";
      src = python3Packages.fetchPypi {
        inherit (oldAttrs) pname;
        inherit version;
        sha256 = "1f4c0fd725c53bnfsnjimxr3p0kq89l038kxnb6wzz6kkmh06mq4";
      };
    }))
    pyyaml
    setproctitle
    setuptools
    sortedcontainers
    websockets
  ]);
}
