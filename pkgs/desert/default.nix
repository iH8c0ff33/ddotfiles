{ fetchFromGitHub, python3Packages }:
python3Packages.buildPythonPackage rec {
  pname = "desert";
  version = "2020.11.18";

  src = python3Packages.fetchPypi {
    inherit pname version;
    sha256 = "0bf6cdyp62fbjsazsdvz0k7riya9fgixfvknbaayqkn83m9gpdyp";
  };

  checkInputs = with python3Packages; [ pytest ];

  propagatedBuildInputs = with python3Packages; [
    attrs
    (marshmallow.overrideAttrs (oldAttrs: rec {
      version = "3.12.1";
      src = fetchPypi {
        inherit (oldAttrs) pname;
        inherit version;
        sha256 = "0h70m4z1kbcwsd0sv8cwlcmpj4dnblvr5vj18j7wa327f1dlfl40";
      };
    }))
    typing-inspect
  ];
}
