{ buildGoModule, fetchFromGitHub, stdenv }:
let
  version = "0.0.1";

  kcfishgen = buildGoModule {
    pname = "kcfishgen";
    inherit version;

    src = fetchFromGitHub {
      owner = "evanlucas";
      repo = "fish-kubectl-completions";
      rev = "bbe3b831bcf8c0511fceb0607e4e7511c73e8c71";
      sha256 = "1r6wqvvvb755jkmlng1i085s7cj1psxmddqghm80x5573rkklfps";
    };

    vendorSha256 = "f3b3259c0751f5c14ff5684a236f1106697897156075ae283676cf8604a39fd2";
  };
in
stdenv.mkDerivation rec {
  pname = "fish-kubectl-completions";
  inherit version;

  nativeBuildInputs = [ kcfishgen ];

  unpackPhase = "true";

  installPhase = ''
    mkdir -p $out/share/fish/vendor_completions.d

    ${kcfishgen}/bin/kcfishgen > $out/share/fish/vendor_completions.d/kubectl.fish
  '';
}
