{
  description = "XeHive's Nix Hive";

  nixConfig = {
    extra-experimental-features = "nix-command flakes";

    extra-substituters = [
      # nix-community
      "https://nix-community.cachix.org"
      # colmena
      "https://colmena.cachix.org"
    ];

    extra-trusted-public-keys = [
      "nix-community.cachix.org-1:mB9FSh9qf2dCimDSUo8Zy7bkq5CX+/rkCWyvRCYg3Fs="
      "colmena.cachix.org-1:7BzpDnjjH8ki2CT3f6GdOk7QAzPOl+1t3LvTLXqYcSg="
    ];
  };

  inputs = {
    nixpkgs-stable.url = "https://flakehub.com/f/NixOS/nixpkgs/*.tar.gz";
    nixpkgs-unstable.url = "https://flakehub.com/f/NixOS/nixpkgs/0.1.0.tar.gz";
    nixpkgs.follows = "nixpkgs-unstable";

    home-manager-stable = {
      url = "https://flakehub.com/f/nix-community/home-manager/*.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs-stable";
    };
    home-manager-unstable = {
      url = "github:nix-community/home-manager";
      inputs.nixpkgs.follows = "nixpkgs-unstable";
    };
    home-manager.follows = "home-manager-unstable";

    lanzaboote = {
      url = "github:nix-community/lanzaboote";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-generators = {
      url =
        "https://flakehub.com/f/nix-community/nixos-generators/0.1.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixos-hardware.url = "github:nixos/nixos-hardware";

    nix-darwin.url = "github:LnL7/nix-darwin";

    haumea = {
      url = "https://flakehub.com/f/nix-community/haumea/0.2.0.tar.gz";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nixago.url = "github:nix-community/nixago";
    nixago.inputs.nixpkgs.follows = "nixpkgs";
    std = {
      url = "github:divnix/std";
      inputs = {
        haumea.follows = "haumea";
        nixpkgs.follows = "nixpkgs";
        nixago.follows = "nixago";
      };
    };

    colmena.url = "github:zhaofengli/colmena";
    colmena.inputs.flake-compat.follows = "";
    hive = {
      url = "github:divnix/hive";
      inputs = {
        nixpkgs.follows = "nixpkgs";
        colmena.follows = "colmena";
        std.follows = "std";
      };
    };

    disko = {
      url = "github:nix-community/disko";
      inputs.nixpkgs.follows = "nixpkgs";
    };

    nix-index-database.url = "github:nix-community/nix-index-database";
    neovim-nightly-overlay = {
      url = "github:nix-community/neovim-nightly-overlay";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, hive, std, ... }@inputs:
    let
      inherit (inputs.nixpkgs.lib) mergeAttrsList;
      inherit (inputs.std) incl;

      blockTypes = mergeAttrsList [ std.blockTypes hive.blockTypes ];

      nr-renamer = _: target: target;
      collect-colmena =
        self.lib.nixos.collectors.colmenaConfigurations nr-renamer;
      collect-nixos = self.lib.nixos.collectors.nixosConfigurations nr-renamer;
      collect-nr = hive.collect // { renamer = nr-renamer; };
    in hive.growOn {
      inherit inputs;
      cellsFrom = incl ./. [ "common" "darwin" "haumea" "home" "nixos" ];
      cellBlocks = with blockTypes; [
        (functions "lib")

        # SSH pubkeys
        (functions "pubkeys")

        # NixOS/nix-darwin/home-manager modules
        (functions "modules")
        (functions "profiles")
        (functions "suites")
        (functions "users")

        # nixpkgs (bee) configs
        (functions "channels")
        (functions "nixpkgsConfig")

        # NixOS/nix-darwin/home-manager configurations
        colmenaConfigurations
        darwinConfigurations
        diskoConfigurations
        (functions "hardwareConfigurations")
        homeConfigurations
        nixosConfigurations
        (functions "nixosModules")

        (functions "generators")
        (pkgs "pkgs")
      ];
    } { lib = { nixos = hive.pick self [[ "nixos" "lib" ]]; }; } {
      packages = hive.harvest self [ "nixos" "pkgs" ];
    } { homeModules = hive.pick self [[ "home" "modules" ]]; } {
      colmenaHive = collect-colmena self;
      darwinConfigurations = collect-nr self "darwinConfigurations";
      homeConfigurations = collect-nr self "homeConfigurations";
      nixosConfigurations = collect-nixos self;
    };
}
