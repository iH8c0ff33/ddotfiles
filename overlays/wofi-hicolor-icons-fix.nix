final: prev: {
  wofi = prev.wofi.overrideAttrs (oldAttrs: {
    buildInputs = oldAttrs.buildInputs ++ [ prev.hicolor-icon-theme ];
  });
}
