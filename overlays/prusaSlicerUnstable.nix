final: prev:
{
  prusa-slicer-unstable = prev.prusa-slicer.overrideAttrs (
    old: {
      version = "2.3.0-rc3_bugfix.1";

      buildInputs = prev.prusa-slicer.buildInputs ++ [ prev.dbus prev.gtk2 ];

      patches = ./prusaSlicer_fix.patch;

      preConfigure = prev.glib.flattenInclude;

      src = prev.fetchFromGitHub {
        owner = "prusa3d";
        repo = "PrusaSlicer";
        sha256 = "19nqxa00wz5vm4jji0qwrw4yq14xmj08ydm9mvznkdg9nb32girk";
        rev = "b7bfaea1ba9a45b9835b741b9439e26bb8ddbcab";
      };
    }
  );
}
