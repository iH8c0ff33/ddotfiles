
{ config, profiles, pkgs, suites, users, ... }:
{
  imports = [ ./hardware-configuration.nix profiles.network.ssh profiles.shell.fish users.daniele ];

  # Use the systemd-boot EFI boot loader.
  boot.loader.systemd-boot.enable = true;
  boot.loader.efi.canTouchEfiVariables = true;

  # ZFS
  boot.supportedFilesystems = [ "zfs" ];
  boot.kernelPackages = config.boot.zfs.package.latestCompatibleLinuxPackages;
  boot.zfs = {
    devNodes = "/dev/disk/by-id";
    forceImportRoot = false;
    extraPools = [ "data" ];
  };
  networking.hostId = "43c09f13";

  ## Cockpit
  services.cockpit.enable = true;

  networking.hostName = "h1";

  ## K3s
  services.k3s = {
    enable = true;
    extraFlags = "--disable traefik --disable-helm-controller --tls-san h1.hydra-carp.ts.net --tls-san 100.80.254.119";
    package = pkgs.k3s_1_29;
  };
  # Allow CNI interfaces (needed for k3s)
  networking.firewall.trustedInterfaces = [ "cni+" ];
  networking.vlans.lbsvc = {
    id = 4;
    interface = "enp4s0";
  };

  ## Tailscale
  services.tailscale.enable = true;
  networking.firewall = {
    checkReversePath = "loose";
    allowedTCPPorts = [ 6443 ];
    allowedUDPPorts = [ 8472 ];
    interfaces."tailscale0".allowedTCPPorts = [ 80 443 6443 8123 25565 ];
  };

  # This host is a server
  services.xserver.displayManager.gdm.autoSuspend = false;

  system.stateVersion = "22.11";
}
